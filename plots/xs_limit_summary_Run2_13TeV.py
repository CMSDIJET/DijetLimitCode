#!/usr/bin/env python

import sys, os, subprocess, string, re
from ROOT import *
from array import array
import numpy as np
import CMS_lumi


gROOT.SetBatch(kTRUE);
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)
gStyle.SetTitleFont(42, "XYZ")
gStyle.SetTitleSize(0.06, "XYZ")
gStyle.SetLabelFont(42, "XYZ")
gStyle.SetLabelSize(0.05, "XYZ")
gStyle.SetCanvasBorderMode(0)
gStyle.SetFrameBorderMode(0)
gStyle.SetCanvasColor(kWhite)
gStyle.SetPadTickX(1)
gStyle.SetPadTickY(1)
gStyle.SetPadLeftMargin(0.15)
gStyle.SetPadRightMargin(0.05)
gStyle.SetPadTopMargin(0.06)
gStyle.SetPadBottomMargin(0.14)
gROOT.ForceStyle()

masses = array('d')
xs_obs_limits = array('d')
xs_exp_limits = array('d')
masses_exp = array('d')
xs_exp_limits_1sigma = array('d')
xs_exp_limits_1sigma_up = array('d')
xs_exp_limits_2sigma = array('d')
xs_exp_limits_2sigma_up = array('d')


syst = True
#syst = False

useTeV = True
#useTeV = False

masses = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0])

xs_gg_obs_limits = array('d', [2.47509, 3.83556, 24.38, 13.3815, 4.18611, 2.17485, 1.77723, 1.75765, 1.58693, 1.87754, 2.35201, 1.96082, 1.8162, 1.61099, 1.48962, 1.53807, 1.30311])
xs_gg_exp_limits = array('d', [9.20649, 7.58136, 5.5474, 4.84689, 3.943555, 3.34593, 3.052885, 2.730025, 2.4789, 2.155165, 1.74733, 1.624335, 1.55666, 1.40621, 1.271375, 1.162985, 1.09702])

xs_qg_obs_limits = array('d', [3.40799, 6.21295, 13.9264, 5.70782, 2.19612, 1.56268, 1.3938, 1.34968, 1.35388, 1.70711, 1.70864, 1.858, 1.17723, 1.11691, 1.09074, 0.990288, 0.791434])
xs_qg_exp_limits = array('d', [6.07531, 4.85117, 3.819335, 3.370985, 2.86341, 2.42906, 2.1546, 1.918275, 1.7454, 1.56185, 1.33594, 1.177, 1.10291, 0.963783, 0.8798835, 0.838167, 0.800876])

xs_qq_obs_limits = array('d', [3.97496, 8.63273, 6.8204, 3.07626, 1.67548, 1.25714, 1.15667, 1.11332, 1.2765, 1.8935, 1.70738, 1.24373, 0.932713, 0.874972, 0.894555, 0.762036, 0.531577])
xs_qq_exp_limits = array('d', [4.552785, 3.54086, 3.20133, 2.71352, 2.214, 1.897145, 1.70106, 1.561165, 1.380675, 1.166545, 1.02438, 0.965802, 0.862712, 0.786767, 0.7418015, 0.669594, 0.607163])

if syst:
  xs_gg_obs_limits = array('d', [7.62311, 12.4745, 38.9157, 25.6973, 11.0892, 5.50526, 4.54439, 4.17164, 3.74474, 4.02983, 4.83448, 4.34305, 3.67513, 3.07661, 2.93706, 2.80332, 2.45866])
  xs_gg_exp_limits = array('d', [24.94525, 20.0278, 14.0946, 12.347, 10.0453, 8.03789, 6.86855, 5.82553, 4.882315, 4.15679, 3.57756, 3.279675, 3.0594, 2.82412, 2.53698, 2.339045, 2.200505])

  xs_qg_obs_limits = array('d', [11.7714, 16.9776, 22.0275, 11.6582, 5.35112, 3.55859, 2.85859, 2.75599, 2.60531, 2.94131, 3.25826, 2.62847, 2.16147, 1.91004, 1.7581, 1.6015, 1.37303])
  xs_qg_exp_limits = array('d', [16.26235, 12.7514, 9.2586, 7.807355, 6.31827, 5.11768, 4.27029, 3.65319, 3.03223, 2.53214, 2.19563, 2.019015, 1.87499, 1.70662, 1.56985, 1.48363, 1.39514])

  xs_qq_obs_limits = array('d', [9.90295, 14.5168, 10.3987, 5.68074, 3.33093, 2.43718, 1.97698, 1.94017, 2.02482, 2.4409, 2.30197, 1.73818, 1.34864, 1.23991, 1.19888, 1.06035, 0.868413])
  xs_qq_exp_limits = array('d', [11.3328, 8.31654, 6.11933, 4.971535, 4.14249, 3.44584, 2.86092, 2.385205, 2.000665, 1.68192, 1.5038, 1.38957, 1.25428, 1.127585, 1.05383, 1.00789, 0.9220155])

# rescale the limits to the correct integrated luminosity
scaleFactor = 1866./1914.

xs_gg_obs_limits = array('d',(np.array(xs_gg_obs_limits.tolist())*scaleFactor).tolist())
xs_gg_exp_limits = array('d',(np.array(xs_gg_exp_limits.tolist())*scaleFactor).tolist())

xs_qg_obs_limits = array('d',(np.array(xs_qg_obs_limits.tolist())*scaleFactor).tolist())
xs_qg_exp_limits = array('d',(np.array(xs_qg_exp_limits.tolist())*scaleFactor).tolist())

xs_qq_obs_limits = array('d',(np.array(xs_qq_obs_limits.tolist())*scaleFactor).tolist())
xs_qq_exp_limits = array('d',(np.array(xs_qq_exp_limits.tolist())*scaleFactor).tolist())

#------------------------------------------------------
# theory curves: gg
massesS8 = array('d', [1000.0,1100.0,1200.0,1300.0,1400.0,1500.0,1600.0,1700.0,1800.0,1900.0,2000.0,2100.0,2200.0,2300.0,2400.0,2500.0,2600.0,2700.0,2800.0,2900.0,3000.0,3100.0,3200.0,3300.0,3400.0,3500.0,3600.0,3700.0,3800.0,3900.0,4000.0,4100.0,4200.0,4300.0,4400.0,4500.0,4600.0,4700.0,4800.0,4900.0,5000.0,5100.0,5200.0,5300.0,5400.0,5500.0,5600.0,5700.0,5800.0,5900.0,6000.0])
xsS8 = array('d', [5.15E+02,2.93E+02,1.73E+02,1.11E+02,6.68E+01,4.29E+01,2.86E+01,1.90E+01,1.30E+01,8.71E+00,6.07E+00,4.32E+00,2.99E+00,2.14E+00,1.53E+00,1.09E+00,8.10E-01,5.83E-01,4.38E-01,3.25E-01,2.43E-01,1.78E-01,1.37E-01,1.03E-01,7.66E-02,5.76E-02,4.46E-02,3.42E-02,2.60E-02,1.94E-02,1.50E-02,1.20E-02,9.12E-03,6.99E-03,5.47E-03,4.19E-03,3.21E-03,2.53E-03,1.90E-03,1.50E-03,1.18E-03,9.13E-04,7.07E-04,5.60E-04,4.35E-04,3.36E-04,2.59E-04,2.09E-04,1.59E-04,1.21E-04,9.38E-05])

xs_maxS8 = 2e+03
idxS8 = 0

for i, xs in enumerate(xsS8):
  if xs < xs_maxS8:
    idxS8 = i
    break

#------------------------------------------------------
# theory curves: qg
massesQstar = array('d', [500.0,550.0,600.0,650.0,700.0,750.0,800.0,850.0,900.0,950.0,1000.0,1100.0,1200.0,1300.0,1400.0,1500.0,1600.0,1700.0,1800.0,1900.0,2000.0,2100.0,2200.0,2300.0,2400.0,2500.0,2600.0,2700.0,2800.0,2900.0,3000.0,3100.0,3200.0,3300.0,3400.0,3500.0,3600.0,3700.0,3800.0,3900.0,4000.0,4100.0,4200.0,4300.0,4400.0,4500.0,4600.0,4700.0,4800.0,4900.0,5000.0,5100.0,5200.0,5300.0,5400.0,5500.0,5600.0,5700.0,5800.0,5900.0,6000.0,6100.0,6200.0,6300.0,6400.0,6500.0,6600.0,6700.0,6800.0,6900.0,7000.0,7100.0,7200.0,7300.0,7400.0,7500.0,7600.0,7700.0,7800.0,7900.0,8000.0,8100.0,8200.0,8300.0,8400.0,8500.0,8600.0,8700.0,8800.0,8900.0,9000.0])
xsQstar = array('d', [0.7483E+04,0.5185E+04,0.3676E+04,0.2664E+04,0.1963E+04,0.1468E+04,0.1114E+04,0.8546E+03,0.6628E+03,0.5190E+03,0.4101E+03,0.2620E+03,0.1721E+03,0.1157E+03,0.7934E+02,0.5540E+02,0.3928E+02,0.2823E+02,0.2054E+02,0.1510E+02,0.1121E+02,0.8390E+01,0.6328E+01,0.4807E+01,0.3674E+01,0.2824E+01,0.2182E+01,0.1694E+01,0.1320E+01,0.1033E+01,0.8116E+00,0.6395E+00,0.5054E+00,0.4006E+00,0.3182E+00,0.2534E+00,0.2022E+00,0.1616E+00,0.1294E+00,0.1038E+00,0.8333E-01,0.6700E-01,0.5392E-01,0.4344E-01,0.3503E-01,0.2827E-01,0.2283E-01,0.1844E-01,0.1490E-01,0.1205E-01,0.9743E-02,0.7880E-02,0.6373E-02,0.5155E-02,0.4169E-02,0.3371E-02,0.2725E-02,0.2202E-02,0.1779E-02,0.1437E-02,0.1159E-02,0.9353E-03,0.7541E-03,0.6076E-03,0.4891E-03,0.3935E-03,0.3164E-03,0.2541E-03,0.2039E-03,0.1635E-03,0.1310E-03,0.1049E-03,0.8385E-04,0.6699E-04,0.5347E-04,0.4264E-04,0.3397E-04,0.2704E-04,0.2151E-04,0.1709E-04,0.1357E-04,0.1077E-04,0.8544E-05,0.6773E-05,0.5367E-05,0.4251E-05,0.3367E-05,0.2666E-05,0.2112E-05,0.1673E-05,0.1326E-05])

xs_maxQstar = 2e+03
idxQstar = 0

for i, xs in enumerate(xsQstar):
  if xs < xs_maxQstar:
    idxQstar = i
    break

#------------------------------------------------------
# theory curves: qq
massesTh = array('d', [500.0,550.0,600.0,650.0,700.0,750.0,800.0,850.0,900.0,950.0,1000.0,1100.0,1200.0,1300.0,1400.0,1500.0,1600.0,1700.0,1800.0,1900.0,2000.0,2100.0,2200.0,2300.0,2400.0,2500.0,2600.0,2700.0,2800.0,2900.0,3000.0,3100.0,3200.0,3300.0,3400.0,3500.0,3600.0,3700.0,3800.0,3900.0,4000.0,4100.0,4200.0,4300.0,4400.0,4500.0,4600.0,4700.0,4800.0,4900.0,5000.0,5100.0,5200.0,5300.0,5400.0,5500.0,5600.0,5700.0,5800.0,5900.0,6000.0,6100.0,6200.0,6300.0,6400.0,6500.0,6600.0,6700.0,6800.0,6900.0,7000.0,7100.0,7200.0,7300.0,7400.0,7500.0,7600.0,7700.0,7800.0,7900.0,8000.0,8100.0,8200.0,8300.0,8400.0,8500.0,8600.0,8700.0,8800.0,8900.0,9000.0])

xsAxi = array('d', [0.2492E+04,0.1760E+04,0.1274E+04,0.9436E+03,0.7115E+03,0.5448E+03,0.4232E+03,0.3325E+03,0.2641E+03,0.2117E+03,0.1712E+03,0.1144E+03,0.7846E+02,0.5497E+02,0.3921E+02,0.2842E+02,0.2086E+02,0.1550E+02,0.1163E+02,0.8802E+01,0.6713E+01,0.5157E+01,0.3984E+01,0.3095E+01,0.2416E+01,0.1894E+01,0.1491E+01,0.1177E+01,0.9328E+00,0.7410E+00,0.5901E+00,0.4710E+00,0.3767E+00,0.3019E+00,0.2423E+00,0.1947E+00,0.1567E+00,0.1262E+00,0.1017E+00,0.8207E-01,0.6626E-01,0.5352E-01,0.4324E-01,0.3494E-01,0.2824E-01,0.2283E-01,0.1845E-01,0.1490E-01,0.1203E-01,0.9711E-02,0.7833E-02,0.6314E-02,0.5085E-02,0.4091E-02,0.3288E-02,0.2639E-02,0.2115E-02,0.1693E-02,0.1353E-02,0.1079E-02,0.8594E-03,0.6830E-03,0.5417E-03,0.4286E-03,0.3383E-03,0.2664E-03,0.2092E-03,0.1638E-03,0.1279E-03,0.9958E-04,0.7727E-04,0.5977E-04,0.4606E-04,0.3537E-04,0.2707E-04,0.2064E-04,0.1567E-04,0.1185E-04,0.8929E-05,0.6698E-05,0.5005E-05,0.3724E-05,0.2760E-05,0.2037E-05,0.1498E-05,0.1097E-05,0.8002E-06,0.5820E-06,0.4218E-06,0.3046E-06,0.2195E-06])
xsDiquark = array('d', [0.4952E+03,0.3745E+03,0.2888E+03,0.2268E+03,0.1808E+03,0.1460E+03,0.1193E+03,0.9843E+02,0.8198E+02,0.6884E+02,0.5824E+02,0.4250E+02,0.3172E+02,0.2411E+02,0.1862E+02,0.1457E+02,0.1153E+02,0.9211E+01,0.7419E+01,0.6019E+01,0.4912E+01,0.4031E+01,0.3323E+01,0.2750E+01,0.2284E+01,0.1903E+01,0.1590E+01,0.1331E+01,0.1117E+01,0.9386E+00,0.7900E+00,0.6658E+00,0.5618E+00,0.4745E+00,0.4010E+00,0.3391E+00,0.2869E+00,0.2428E+00,0.2055E+00,0.1740E+00,0.1473E+00,0.1246E+00,0.1055E+00,0.8922E-01,0.7544E-01,0.6376E-01,0.5385E-01,0.4546E-01,0.3834E-01,0.3231E-01,0.2720E-01,0.2288E-01,0.1922E-01,0.1613E-01,0.1352E-01,0.1132E-01,0.9463E-02,0.7900E-02,0.6584E-02,0.5479E-02,0.4551E-02,0.3774E-02,0.3124E-02,0.2581E-02,0.2128E-02,0.1750E-02,0.1437E-02,0.1177E-02,0.9612E-03,0.7833E-03,0.6366E-03,0.5160E-03,0.4170E-03,0.3360E-03,0.2700E-03,0.2162E-03,0.1725E-03,0.1372E-03,0.1087E-03,0.8577E-04,0.6742E-04,0.5278E-04,0.4114E-04,0.3192E-04,0.2465E-04,0.1894E-04,0.1448E-04,0.1101E-04,0.8322E-05,0.6253E-05,0.4670E-05])
xsWprime = array('d', [0.1063E+03,0.7731E+02,0.5744E+02,0.4351E+02,0.3348E+02,0.2611E+02,0.2062E+02,0.1645E+02,0.1325E+02,0.1076E+02,0.8811E+01,0.6024E+01,0.4216E+01,0.3010E+01,0.2185E+01,0.1610E+01,0.1200E+01,0.9043E+00,0.6875E+00,0.5271E+00,0.4067E+00,0.3158E+00,0.2464E+00,0.1932E+00,0.1521E+00,0.1201E+00,0.9512E-01,0.7554E-01,0.6012E-01,0.4792E-01,0.3827E-01,0.3059E-01,0.2448E-01,0.1960E-01,0.1571E-01,0.1259E-01,0.1009E-01,0.8090E-02,0.6483E-02,0.5193E-02,0.4158E-02,0.3327E-02,0.2660E-02,0.2125E-02,0.1695E-02,0.1351E-02,0.1075E-02,0.8546E-03,0.6781E-03,0.5372E-03,0.4248E-03,0.3353E-03,0.2642E-03,0.2077E-03,0.1629E-03,0.1275E-03,0.9957E-04,0.7757E-04,0.6027E-04,0.4670E-04,0.3610E-04,0.2783E-04,0.2140E-04,0.1641E-04,0.1254E-04,0.9561E-05,0.7269E-05,0.5510E-05,0.4167E-05,0.3143E-05,0.2364E-05,0.1774E-05,0.1329E-05,0.9931E-06,0.7411E-06,0.5523E-06,0.4108E-06,0.3055E-06,0.2271E-06,0.1687E-06,0.1254E-06,0.9327E-07,0.6945E-07,0.5177E-07,0.3863E-07,0.2888E-07,0.2162E-07,0.1622E-07,0.1218E-07,0.9156E-08,0.6893E-08])
xsZprime = array('d', [0.6574E+02,0.4728E+02,0.3476E+02,0.2609E+02,0.1990E+02,0.1540E+02,0.1207E+02,0.9565E+01,0.7653E+01,0.6178E+01,0.5027E+01,0.3398E+01,0.2353E+01,0.1663E+01,0.1196E+01,0.8729E+00,0.6450E+00,0.4822E+00,0.3638E+00,0.2769E+00,0.2123E+00,0.1639E+00,0.1272E+00,0.9933E-01,0.7789E-01,0.6134E-01,0.4848E-01,0.3845E-01,0.3059E-01,0.2440E-01,0.1952E-01,0.1564E-01,0.1256E-01,0.1010E-01,0.8142E-02,0.6570E-02,0.5307E-02,0.4292E-02,0.3473E-02,0.2813E-02,0.2280E-02,0.1848E-02,0.1499E-02,0.1216E-02,0.9864E-03,0.8002E-03,0.6490E-03,0.5262E-03,0.4264E-03,0.3453E-03,0.2795E-03,0.2260E-03,0.1826E-03,0.1474E-03,0.1188E-03,0.9566E-04,0.7690E-04,0.6173E-04,0.4947E-04,0.3957E-04,0.3159E-04,0.2516E-04,0.2001E-04,0.1587E-04,0.1255E-04,0.9906E-05,0.7795E-05,0.6116E-05,0.4785E-05,0.3731E-05,0.2900E-05,0.2247E-05,0.1734E-05,0.1334E-05,0.1022E-05,0.7804E-06,0.5932E-06,0.4492E-06,0.3388E-06,0.2544E-06,0.1903E-06,0.1417E-06,0.1051E-06,0.7764E-07,0.5711E-07,0.4186E-07,0.3055E-07,0.2223E-07,0.1612E-07,0.1164E-07,0.8394E-08])
xsRSG = array('d', [0.2534E+03,0.1632E+03,0.1085E+03,0.7421E+02,0.5193E+02,0.3707E+02,0.2695E+02,0.1990E+02,0.1490E+02,0.1130E+02,0.8673E+01,0.5261E+01,0.3306E+01,0.2139E+01,0.1420E+01,0.9639E+00,0.6666E+00,0.4691E+00,0.3349E+00,0.2424E+00,0.1774E+00,0.1312E+00,0.9793E-01,0.7373E-01,0.5591E-01,0.4269E-01,0.3279E-01,0.2533E-01,0.1966E-01,0.1532E-01,0.1200E-01,0.9422E-02,0.7426E-02,0.5870E-02,0.4652E-02,0.3696E-02,0.2942E-02,0.2346E-02,0.1874E-02,0.1499E-02,0.1200E-02,0.9625E-03,0.7724E-03,0.6202E-03,0.4983E-03,0.4005E-03,0.3220E-03,0.2589E-03,0.2082E-03,0.1673E-03,0.1345E-03,0.1080E-03,0.8669E-04,0.6953E-04,0.5573E-04,0.4462E-04,0.3568E-04,0.2850E-04,0.2273E-04,0.1810E-04,0.1439E-04,0.1142E-04,0.9042E-05,0.7147E-05,0.5635E-05,0.4433E-05,0.3479E-05,0.2722E-05,0.2125E-05,0.1653E-05,0.1282E-05,0.9914E-06,0.7639E-06,0.5866E-06,0.4489E-06,0.3423E-06,0.2599E-06,0.1966E-06,0.1481E-06,0.1112E-06,0.8308E-07,0.6183E-07,0.4584E-07,0.3384E-07,0.2489E-07,0.1823E-07,0.1330E-07,0.9672E-08,0.7008E-08,0.5059E-08,0.3645E-08])

massesRSGgg = array('d')
xsRSGgg = array('d')
fractionRSGgg = [0.27903,0.27503,0.27121,0.26705,0.26295,0.25870,0.25433,0.25003,0.24566,0.24125,0.23673,0.22778,0.21881,0.20990,0.20108,0.19242,0.18393,0.17570,0.16771,0.16002,0.15260,0.14551,0.13870,0.13223,0.12604,0.12017,0.11459,0.10929,0.10428,0.09951,0.09501,0.09074,0.08670,0.08287,0.07924,0.07582,0.07257,0.06949,0.06658,0.06381,0.06120,0.05872,0.05636,0.05413,0.05201,0.05001,0.04810,0.04629,0.04457,0.04295,0.04140,0.03993,0.03854,0.03721,0.03596,0.03478,0.03364,0.03258,0.03157,0.03061,0.02971,0.02885,0.02805,0.02729,0.02657,0.02589,0.02526,0.02466,0.02411,0.02358,0.02309,0.02264,0.02221,0.02182,0.02145,0.02111,0.02079,0.02049,0.02021,0.01994,0.01969,0.01944,0.01920,0.01895,0.01870,0.01844,0.01816,0.01785,0.01751,0.01713,0.01670]

for i in range(0,len(fractionRSGgg)):
  massesRSGgg.append(massesTh[i])
  xsRSGgg.append( xsRSG[i]*fractionRSGgg[i] )


xs_maxQQ = 2e+03
idxAxi = 0
idxDiquark = 0

for i, xs in enumerate(xsAxi):
  if xs < xs_maxQQ:
    idxAxi = i
    break

for i, xs in enumerate(xsDiquark):
  if xs < xs_maxQQ:
    idxDiquark = i
    break

#------------------------------------------------------
if useTeV:
  masses = array('d',(np.array(masses.tolist())/1000.).tolist())
  masses_exp = array('d',(np.array(masses_exp.tolist())/1000.).tolist())
  massesS8 = array('d',(np.array(massesS8.tolist())/1000.).tolist())
  massesQstar = array('d',(np.array(massesQstar.tolist())/1000.).tolist())
  massesTh = array('d',(np.array(massesTh.tolist())/1000.).tolist())
  massesRSGgg = array('d',(np.array(massesRSGgg.tolist())/1000.).tolist())

# theory curves: gg
graph_xsS8 = TGraph(len(massesS8[idxS8:-1]),massesS8[idxS8:-1],xsS8[idxS8:-1])
graph_xsS8.SetLineWidth(3)
graph_xsS8.SetLineStyle(6)
graph_xsS8.SetLineColor(6)

# theory curves: qg
graph_xsQstar = TGraph(len(massesQstar[idxQstar:-1]),massesQstar[idxQstar:-1],xsQstar[idxQstar:-1])
graph_xsQstar.SetLineWidth(3)
graph_xsQstar.SetLineStyle(2)
graph_xsQstar.SetLineColor(1)

# theory curves: qq
graph_xsAxi = TGraph(len(massesTh[idxAxi:-1]),massesTh[idxAxi:-1],xsAxi[idxAxi:-1])
graph_xsAxi.SetLineWidth(3)
graph_xsAxi.SetLineStyle(3)
graph_xsAxi.SetLineColor(63)

graph_xsDiquark = TGraph(len(massesTh[idxDiquark:-1]),massesTh[idxDiquark:-1],xsDiquark[idxDiquark:-1])
graph_xsDiquark.SetLineWidth(3)
graph_xsDiquark.SetLineStyle(9)
graph_xsDiquark.SetLineColor(42)

graph_xsWprime = TGraph(len(massesTh),massesTh,xsWprime)
graph_xsWprime.SetLineWidth(3)
graph_xsWprime.SetLineStyle(7)
graph_xsWprime.SetLineColor(46)

graph_xsZprime = TGraph(len(massesTh),massesTh,xsZprime)
graph_xsZprime.SetLineWidth(3)
graph_xsZprime.SetLineStyle(5)
graph_xsZprime.SetLineColor(38)

graph_xsRSG = TGraph(len(massesTh),massesTh,xsRSG)
graph_xsRSG.SetLineWidth(3)
graph_xsRSG.SetLineStyle(4)
graph_xsRSG.SetLineColor(30)

graph_xsRSGgg = TGraph(len(massesRSGgg),massesRSGgg,xsRSGgg)
graph_xsRSGgg.SetLineWidth(3)
graph_xsRSGgg.SetLineStyle(8)
graph_xsRSGgg.SetLineColor(32)

#------------------------------------------------------
graph_exp_gg = TGraph(len(masses),masses,xs_gg_exp_limits)
graph_exp_gg.SetMarkerStyle(24)
graph_exp_gg.SetMarkerColor(TColor.GetColor("#006600"))
graph_exp_gg.SetLineWidth(3)
#graph_exp_gg.SetLineStyle(2)
graph_exp_gg.SetLineColor(TColor.GetColor("#006600"))
#graph_exp_gg.GetXaxis().SetTitle("Resonance mass [GeV]")
#graph_exp_gg.GetYaxis().SetTitle("#sigma #it{B} #it{A} [pb]")
#graph_exp_gg.GetYaxis().SetTitleOffset(1.1)
#graph_exp_gg.GetYaxis().SetRangeUser(1e-02,1e+03)

graph_obs_gg = TGraph(len(masses),masses,xs_gg_obs_limits)
graph_obs_gg.SetMarkerStyle(24)
graph_obs_gg.SetMarkerColor(TColor.GetColor("#006600"))
graph_obs_gg.SetLineWidth(3)
#graph_obs_gg.SetLineStyle(1)
graph_obs_gg.SetLineColor(TColor.GetColor("#006600"))
graph_obs_gg.GetXaxis().SetTitle("Resonance mass [" + ("TeV" if useTeV else "GeV") + "]")
graph_obs_gg.GetYaxis().SetTitle("#sigma #it{B} #it{A} [pb]")
graph_obs_gg.GetYaxis().SetTitleOffset(1.1)
graph_obs_gg.GetYaxis().SetRangeUser(1e-01,1e+04)

graph_exp_qg = TGraph(len(masses),masses,xs_qg_exp_limits)
graph_exp_qg.SetMarkerStyle(20)
graph_exp_qg.SetMarkerColor(2)
graph_exp_qg.SetLineWidth(3)
#graph_exp_qg.SetLineStyle(2)
graph_exp_qg.SetLineColor(2)

graph_obs_qg = TGraph(len(masses),masses,xs_qg_obs_limits)
graph_obs_qg.SetMarkerStyle(20)
graph_obs_qg.SetMarkerColor(2)
graph_obs_qg.SetLineWidth(3)
#graph_obs_qg.SetLineStyle(1)
graph_obs_qg.SetLineColor(2)

graph_exp_qq = TGraph(len(masses),masses,xs_qq_exp_limits)
graph_exp_qq.SetMarkerStyle(26)
graph_exp_qq.SetMarkerColor(4)
graph_exp_qq.SetLineWidth(3)
#graph_exp_qq.SetLineStyle(2)
graph_exp_qq.SetLineColor(4)

graph_obs_qq = TGraph(len(masses),masses,xs_qq_obs_limits)
graph_obs_qq.SetMarkerStyle(26)
graph_obs_qq.SetMarkerColor(4)
graph_obs_qq.SetLineWidth(3)
#graph_obs_qq.SetLineStyle(1)
graph_obs_qq.SetLineColor(4)


c = TCanvas("c", "",800,800)
c.cd()

graph_obs_gg.Draw("ALP")
graph_obs_qg.Draw("LP")
graph_obs_qq.Draw("LP")
graph_xsQstar.Draw("L")
graph_xsAxi.Draw("L")
graph_xsDiquark.Draw("L")
#graph_xsS8.Draw("L")
graph_xsWprime.Draw("L")
graph_xsZprime.Draw("L")
graph_xsRSG.Draw("L")
graph_xsRSGgg.Draw("L")

legend = TLegend(.19,.18,.49,.30)
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetFillStyle(0)
legend.SetTextFont(42)
legend.SetTextSize(0.03)
legend.SetMargin(0.20)
legend.SetHeader('95% CL upper limits')
legend.AddEntry(graph_exp_gg,"gluon-gluon","lp")
legend.AddEntry(graph_exp_qg,"quark-gluon","lp")
legend.AddEntry(graph_exp_qq,"quark-quark","lp")
legend.Draw()

legendTh = TLegend(.55,.69,.88,.91)
legendTh.SetBorderSize(0)
legendTh.SetFillColor(0)
legendTh.SetFillStyle(0)
legendTh.SetTextFont(42)
legendTh.SetTextSize(0.03)
legendTh.SetMargin(0.20)
legendTh.AddEntry(graph_xsQstar,"Excited quark","l")
legendTh.AddEntry(graph_xsAxi,"Axigluon/coloron","l")
legendTh.AddEntry(graph_xsDiquark,"Scalar diquark","l")
#legendTh.AddEntry(graph_xsS8,"Color-octet scalar","l")
legendTh.AddEntry(graph_xsRSG,"RS graviton","l")
legendTh.AddEntry(graph_xsWprime,"W'","l")
legendTh.AddEntry(graph_xsZprime,"Z'","l")
legendTh.AddEntry(graph_xsRSGgg,"RS graviton (gg#rightarrowG#rightarrowgg)","l")
legendTh.Draw()

#draw the lumi text on the canvas
CMS_lumi.extraText = "Preliminary"
#CMS_lumi.extraText = ""
CMS_lumi.lumi_sqrtS = "1.9 fb^{-1} (13 TeV)" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)
iPos = 11
if( iPos==0 ): CMS_lumi.relPosX = 0.15
iPeriod = 0

CMS_lumi.CMS_lumi(c, iPeriod, iPos)

gPad.RedrawAxis()

c.SetLogy()
c.SaveAs('xs_limit_DijetLimitCode_summary' + ('_NoSyst' if not syst else '') + '_Run2_Scouting_13TeV_DATA_1914_invpb.eps')
