#!/usr/bin/env python

import string, re
from ROOT import *
from array import array
import numpy as np
import CMS_lumi


gROOT.SetBatch(kTRUE);
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)
gStyle.SetTitleFont(42, "XYZ")
gStyle.SetTitleSize(0.06, "XYZ")
gStyle.SetLabelFont(42, "XYZ")
gStyle.SetLabelSize(0.05, "XYZ")
gStyle.SetCanvasBorderMode(0)
gStyle.SetFrameBorderMode(0)
gStyle.SetCanvasColor(kWhite)
gStyle.SetPadTickX(1)
gStyle.SetPadTickY(1)
gStyle.SetPadLeftMargin(0.15)
gStyle.SetPadRightMargin(0.05)
gStyle.SetPadTopMargin(0.06)
gStyle.SetPadBottomMargin(0.14)
gROOT.ForceStyle()

useTeV = True
#useTeV = False

masses = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0])
xs_stat = array('d', [3.97496, 8.63273, 6.8204, 3.07626, 1.67548, 1.25714, 1.15667, 1.11332, 1.2765, 1.8935, 1.70738, 1.24373, 0.932713, 0.874972, 0.894555, 0.762036, 0.531577])

xs_sys_all = array('d', [9.90295, 14.5168, 10.3987, 5.68074, 3.33093, 2.43718, 1.97698, 1.94017, 2.02482, 2.4409, 2.30197, 1.73818, 1.34864, 1.23991, 1.19888, 1.06035, 0.868413])
xs_sys_lumi = array('d', [3.58809, 6.989, 6.81815, 3.0446, 1.58322, 1.21755, 1.14445, 1.09732, 1.23439, 2.00716, 1.82399, 1.19656, 0.901189, 0.877519, 0.882258, 0.754406, 0.526915])
xs_sys_jes = array('d', [3.59558, 6.9564, 6.98877, 3.20823, 1.63431, 1.22506, 1.14637, 1.10197, 1.27335, 1.97891, 1.84662, 1.25161, 0.922281, 0.873671, 0.875524, 0.759818, 0.555482])
xs_sys_jer = array('d', [3.581, 7.13266, 6.81657, 3.0356, 1.58954, 1.22739, 1.14849, 1.09558, 1.23667, 2.01784, 1.81369, 1.20622, 0.91183, 0.879834, 0.887424, 0.746821, 0.526816])
xs_sys_trig = array('d', [4.42536, 7.25751, 6.88465, 3.07191, 1.6039, 1.22779, 1.15177, 1.09989, 1.22864, 2.01977, 1.81911, 1.20074, 0.905579, 0.880702, 0.887897, 0.749962, 0.527808])
xs_sys_allexbkg = array('d', [4.48693, 7.33063, 7.18825, 3.29614, 1.65898, 1.2599, 1.15274, 1.11797, 1.2697, 1.98268, 1.84751, 1.26725, 0.940308, 0.888804, 0.884574, 0.759953, 0.553871])
xs_sys_bkg = array('d', [7.18605, 10.4256, 9.46282, 5.05672, 3.01092, 2.16298, 1.8689, 1.64511, 1.72428, 2.38598, 2.16326, 1.56239, 1.21813, 1.18338, 1.16821, 1.02396, 0.787532])


r_all = array('d')
r_lumi  = array('d')
r_jes  = array('d')
r_jer  = array('d')
r_trig  = array('d')
r_allexbkg  = array('d')
r_bkg  = array('d')

for i in range(0,len(xs_stat)):
  r_all.append( xs_sys_all[i] / xs_stat[i] )
  r_lumi.append( xs_sys_lumi[i] / xs_stat[i] )
  r_jes.append( xs_sys_jes[i] / xs_stat[i] )
  r_jer.append( xs_sys_jer[i] / xs_stat[i] )
  r_trig.append( xs_sys_trig[i] / xs_stat[i] )
  r_allexbkg.append( xs_sys_allexbkg[i] / xs_stat[i] )
  r_bkg.append( xs_sys_bkg[i] / xs_stat[i] )

if useTeV:
  masses = array('d',(np.array(masses.tolist())/1000.).tolist())

g_all = TGraph(len(masses),masses,r_all)
g_all.SetMarkerStyle(20)
g_all.SetMarkerColor(kBlack)
g_all.SetLineWidth(2)
g_all.SetLineStyle(1)
g_all.SetLineColor(kBlack)
g_all.GetXaxis().SetTitle("qq resonance mass [" + ("TeV" if useTeV else "GeV") + "]")
g_all.GetYaxis().SetTitle("Limit ratio")
g_all.GetYaxis().SetTitleOffset(1.1)
g_all.GetYaxis().SetRangeUser(0.5,4.5)
#g_all.GetXaxis().SetNdivisions(1005)

g_lumi = TGraph(len(masses),masses,r_lumi)
g_lumi.SetMarkerStyle(27)
g_lumi.SetMarkerColor(6)
g_lumi.SetLineWidth(2)
g_lumi.SetLineStyle(6)
g_lumi.SetLineColor(6)

g_jes = TGraph(len(masses),masses,r_jes)
g_jes.SetMarkerStyle(25)
g_jes.SetMarkerColor(kRed)
g_jes.SetLineWidth(2)
g_jes.SetLineStyle(5)
g_jes.SetLineColor(kRed)

g_jer = TGraph(len(masses),masses,r_jer)
g_jer.SetMarkerStyle(26)
g_jer.SetMarkerColor(7)
g_jer.SetLineWidth(2)
g_jer.SetLineStyle(6)
g_jer.SetLineColor(7)

g_trig = TGraph(len(masses),masses,r_trig)
g_trig.SetMarkerStyle(24)
g_trig.SetMarkerColor(kGreen+2)
g_trig.SetLineWidth(2)
g_trig.SetLineStyle(2)
g_trig.SetLineColor(kGreen+2)

g_allexbkg = TGraph(len(masses),masses,r_allexbkg)
g_allexbkg.SetMarkerStyle(23)
g_allexbkg.SetMarkerColor(42)
g_allexbkg.SetLineWidth(2)
g_allexbkg.SetLineStyle(4)
g_allexbkg.SetLineColor(42)

g_bkg = TGraph(len(masses),masses,r_bkg)
g_bkg.SetMarkerStyle(22)
g_bkg.SetMarkerColor(kBlue)
g_bkg.SetLineWidth(2)
g_bkg.SetLineStyle(3)
g_bkg.SetLineColor(kBlue)

c = TCanvas("c", "",800,800)
c.cd()

g_all.Draw("ALP")
g_lumi.Draw("LP")
g_jes.Draw("LP")
g_jer.Draw("LP")
g_trig.Draw("LP")
g_allexbkg.Draw("LP")
g_bkg.Draw("LP")

legend = TLegend(.40,.60,.60,.90)
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetFillStyle(0)
legend.SetTextFont(42)
legend.SetTextSize(0.035)
legend.SetMargin(0.20)
legend.SetHeader("Observed limit ratio")
legend.AddEntry(g_all, "All syst / Stat-only","lp")
legend.AddEntry(g_bkg, "Bkg syst / Stat-only","lp")
legend.AddEntry(g_allexbkg, "All except bkg syst / Stat-only","lp")
legend.AddEntry(g_trig, "Trigger syst / Stat-only","lp")
legend.AddEntry(g_jes, "JES syst / Stat-only","lp")
legend.AddEntry(g_jer, "JER syst / Stat-only","lp")
legend.AddEntry(g_lumi, "Lumi syst / Stat-only","lp")

legend.Draw()

#l1 = TLatex()
#l1.SetTextAlign(12)
#l1.SetTextFont(42)
#l1.SetNDC()
#l1.SetTextSize(0.04)
#l1.DrawLatex(0.18,0.43, "CMS Preliminary")
#l1.DrawLatex(0.18,0.35, "#intLdt = 5 fb^{-1}")
#l1.DrawLatex(0.19,0.30, "#sqrt{s} = 7 TeV")
#l1.DrawLatex(0.18,0.25, "|#eta| < 2.5, |#Delta#eta| < 1.3")
#l1.DrawLatex(0.18,0.20, "Wide Jets")

#draw the lumi text on the canvas
CMS_lumi.extraText = "Preliminary"
CMS_lumi.lumi_sqrtS = "1.9 fb^{-1} (13 TeV)" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)
iPos = 11
iPeriod = 0

CMS_lumi.CMS_lumi(c, iPeriod, iPos)

c.SetGridx()
c.SetGridy()

c.SaveAs('xs_limit_ratio_breakdown_DijetLimitCode_qq_Run2_Scouting_13TeV_DATA_1914_invpb.eps')
