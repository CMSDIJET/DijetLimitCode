#!/usr/bin/env python

import string, re
from ROOT import *
from array import array
import numpy as np
import CMS_lumi


gROOT.SetBatch(kTRUE);
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)
gStyle.SetTitleFont(42, "XYZ")
gStyle.SetTitleSize(0.06, "XYZ")
gStyle.SetLabelFont(42, "XYZ")
gStyle.SetLabelSize(0.05, "XYZ")
gStyle.SetCanvasBorderMode(0)
gStyle.SetFrameBorderMode(0)
gStyle.SetCanvasColor(kWhite)
gStyle.SetPadTickX(1)
gStyle.SetPadTickY(1)
gStyle.SetPadLeftMargin(0.15)
gStyle.SetPadRightMargin(0.05)
gStyle.SetPadTopMargin(0.06)
gStyle.SetPadBottomMargin(0.14)
gROOT.ForceStyle()

useTeV = True
#useTeV = False

masses = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0])
xs_stat = array('d', [3.40799, 6.21295, 13.9264, 5.70782, 2.19612, 1.56268, 1.3938, 1.34968, 1.35388, 1.70711, 1.70864, 1.858, 1.17723, 1.11691, 1.09074, 0.990288, 0.791434])

xs_sys_all = array('d', [11.7714, 16.9776, 22.0275, 11.6582, 5.35112, 3.55859, 2.85859, 2.75599, 2.60531, 2.94131, 3.25826, 2.62847, 2.16147, 1.91004, 1.7581, 1.6015, 1.37303])
xs_sys_lumi = array('d', [4.26039, 6.20066, 13.026, 5.69855, 2.20492, 1.50085, 1.36448, 1.30551, 1.31252, 1.78729, 1.74399, 1.40518, 1.19568, 1.07265, 1.0729, 0.995021, 0.757972])
xs_sys_jes = array('d', [4.24899, 6.30481, 12.9913, 6.00791, 2.25485, 1.50435, 1.37846, 1.31041, 1.32934, 1.78877, 1.70221, 1.45535, 1.23025, 1.07992, 1.06972, 0.995135, 0.77931])
xs_sys_jer = array('d', [4.22268, 6.48544, 13.1465, 5.72789, 2.21996, 1.48639, 1.36125, 1.30366, 1.31075, 1.80733, 1.71871, 1.41119, 1.22317, 1.08345, 1.07859, 0.998169, 0.760569])
xs_sys_trig = array('d', [5.7139, 6.74808, 13.1051, 5.78892, 2.25228, 1.49743, 1.36892, 1.30196, 1.32177, 1.79012, 1.74193, 1.41072, 1.20595, 1.07759, 1.0793, 0.9943, 0.76146])
xs_sys_allexbkg = array('d', [5.86188, 7.13862, 13.6591, 6.24303, 2.30805, 1.53579, 1.39734, 1.33995, 1.34184, 1.78884, 1.7187, 1.45422, 1.26133, 1.09757, 1.08219, 1.00948, 0.78319])
xs_sys_bkg = array('d', [10.7325, 12.7759, 18.9384, 10.1778, 5.07353, 3.42332, 2.80541, 2.45199, 2.27097, 2.67912, 3.07238, 2.40665, 1.93182, 1.73645, 1.71134, 1.55984, 1.27644])


r_all = array('d')
r_lumi  = array('d')
r_jes  = array('d')
r_jer  = array('d')
r_trig  = array('d')
r_allexbkg  = array('d')
r_bkg  = array('d')

for i in range(0,len(xs_stat)):
  r_all.append( xs_sys_all[i] / xs_stat[i] )
  r_lumi.append( xs_sys_lumi[i] / xs_stat[i] )
  r_jes.append( xs_sys_jes[i] / xs_stat[i] )
  r_jer.append( xs_sys_jer[i] / xs_stat[i] )
  r_trig.append( xs_sys_trig[i] / xs_stat[i] )
  r_allexbkg.append( xs_sys_allexbkg[i] / xs_stat[i] )
  r_bkg.append( xs_sys_bkg[i] / xs_stat[i] )

if useTeV:
  masses = array('d',(np.array(masses.tolist())/1000.).tolist())

g_all = TGraph(len(masses),masses,r_all)
g_all.SetMarkerStyle(20)
g_all.SetMarkerColor(kBlack)
g_all.SetLineWidth(2)
g_all.SetLineStyle(1)
g_all.SetLineColor(kBlack)
g_all.GetXaxis().SetTitle("qg resonance mass [" + ("TeV" if useTeV else "GeV") + "]")
g_all.GetYaxis().SetTitle("Limit ratio")
g_all.GetYaxis().SetTitleOffset(1.1)
g_all.GetYaxis().SetRangeUser(0.5,4.5)
#g_all.GetXaxis().SetNdivisions(1005)

g_lumi = TGraph(len(masses),masses,r_lumi)
g_lumi.SetMarkerStyle(27)
g_lumi.SetMarkerColor(6)
g_lumi.SetLineWidth(2)
g_lumi.SetLineStyle(6)
g_lumi.SetLineColor(6)

g_jes = TGraph(len(masses),masses,r_jes)
g_jes.SetMarkerStyle(25)
g_jes.SetMarkerColor(kRed)
g_jes.SetLineWidth(2)
g_jes.SetLineStyle(5)
g_jes.SetLineColor(kRed)

g_jer = TGraph(len(masses),masses,r_jer)
g_jer.SetMarkerStyle(26)
g_jer.SetMarkerColor(7)
g_jer.SetLineWidth(2)
g_jer.SetLineStyle(6)
g_jer.SetLineColor(7)

g_trig = TGraph(len(masses),masses,r_trig)
g_trig.SetMarkerStyle(24)
g_trig.SetMarkerColor(kGreen+2)
g_trig.SetLineWidth(2)
g_trig.SetLineStyle(2)
g_trig.SetLineColor(kGreen+2)

g_allexbkg = TGraph(len(masses),masses,r_allexbkg)
g_allexbkg.SetMarkerStyle(23)
g_allexbkg.SetMarkerColor(42)
g_allexbkg.SetLineWidth(2)
g_allexbkg.SetLineStyle(4)
g_allexbkg.SetLineColor(42)

g_bkg = TGraph(len(masses),masses,r_bkg)
g_bkg.SetMarkerStyle(22)
g_bkg.SetMarkerColor(kBlue)
g_bkg.SetLineWidth(2)
g_bkg.SetLineStyle(3)
g_bkg.SetLineColor(kBlue)

c = TCanvas("c", "",800,800)
c.cd()

g_all.Draw("ALP")
g_lumi.Draw("LP")
g_jes.Draw("LP")
g_jer.Draw("LP")
g_trig.Draw("LP")
g_allexbkg.Draw("LP")
g_bkg.Draw("LP")

legend = TLegend(.40,.60,.60,.90)
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetFillStyle(0)
legend.SetTextFont(42)
legend.SetTextSize(0.035)
legend.SetMargin(0.20)
legend.SetHeader("Observed limit ratio")
legend.AddEntry(g_all, "All syst / Stat-only","lp")
legend.AddEntry(g_bkg, "Bkg syst / Stat-only","lp")
legend.AddEntry(g_allexbkg, "All except bkg syst / Stat-only","lp")
legend.AddEntry(g_trig, "Trigger syst / Stat-only","lp")
legend.AddEntry(g_jes, "JES syst / Stat-only","lp")
legend.AddEntry(g_jer, "JER syst / Stat-only","lp")
legend.AddEntry(g_lumi, "Lumi syst / Stat-only","lp")

legend.Draw()

#l1 = TLatex()
#l1.SetTextAlign(12)
#l1.SetTextFont(42)
#l1.SetNDC()
#l1.SetTextSize(0.04)
#l1.DrawLatex(0.18,0.43, "CMS Preliminary")
#l1.DrawLatex(0.18,0.35, "#intLdt = 5 fb^{-1}")
#l1.DrawLatex(0.19,0.30, "#sqrt{s} = 7 TeV")
#l1.DrawLatex(0.18,0.25, "|#eta| < 2.5, |#Delta#eta| < 1.3")
#l1.DrawLatex(0.18,0.20, "Wide Jets")

#draw the lumi text on the canvas
CMS_lumi.extraText = "Preliminary"
CMS_lumi.lumi_sqrtS = "1.9 fb^{-1} (13 TeV)" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)
iPos = 11
iPeriod = 0

CMS_lumi.CMS_lumi(c, iPeriod, iPos)

c.SetGridx()
c.SetGridy()

c.SaveAs('xs_limit_ratio_breakdown_DijetLimitCode_qg_Run2_Scouting_13TeV_DATA_1914_invpb.eps')
