#!/usr/bin/env python

import sys, os, subprocess, string, re, copy
from argparse import ArgumentParser

#----------------------------------------------------------------------
# Start reading input arguments

# usage description
usage = "Example: python plots/xs_limit_Run2_13TeV.py -l scouting_limits_AllSyst -f gg --massrange 700 1500 50 --useTeV --withSyst"

# input parameters
parser = ArgumentParser(description='Script that plots limits for specified mass points',epilog=usage)

results_group = parser.add_mutually_exclusive_group(required=True)
results_group.add_argument("-l", "--logs_path", dest="logs_path",
                           help="Path to log files",
                           metavar="LOGS_PATH")
results_group.add_argument("--run_limits", dest="run_limits", default=False, action="store_true",
                           help="Run limits")
results_group.add_argument("--read_limits", dest="read_limits", default=False, action="store_true",
                           help="Read existing limits")

parser.add_argument("-f", "--final_state", dest="final_state", required=True,
                    help="Final state (e.g. qq, qg, gg)",
                    metavar="FINAL_STATE")

parser.add_argument("--postfix", dest="postfix", default='Run2_Scouting_13TeV_DATA_1914_invpb', help="Postfix for the output plot name (default: %(default)s)")

parser.add_argument("--fileFormat", dest="fileFormat", default='eps', help="Format of the output plot (default: %(default)s)")

parser.add_argument("--extraText", dest="extraText", default='Preliminary', help="Extra text on the plot (default: %(default)s)")

parser.add_argument("--lumi_sqrtS", dest="lumi_sqrtS", default='1.9 fb^{-1} (13 TeV)', help="Integrated luminosity and center-of-mass energy (default: %(default)s)")

parser.add_argument("--withSyst", dest="withSyst", default=False, action="store_true", help="If plotting limits with systematics included")

parser.add_argument("--useTeV", dest="useTeV", default=False, action="store_true", help="Express mass in units of TeV")

mass_group = parser.add_mutually_exclusive_group(required=True)
mass_group.add_argument("--mass",
                        type=int,
                        nargs = '*',
                        default = 1000,
                        help="Mass can be specified as a single value or a whitespace separated list (default: %(default)i)"
                        )
mass_group.add_argument("--massrange",
                        type=int,
                        nargs = 3,
                        help="Define a range of masses to be produced. Format: min max step",
                        metavar = ('MIN', 'MAX', 'STEP')
                        )
mass_group.add_argument("--masslist",
                        help = "List containing mass information"
                        )

args = parser.parse_args()

# End reading input arguments
#----------------------------------------------------------------------

# mass points for which resonance shapes will be produced
input_masses = []

if args.massrange != None:
    MIN, MAX, STEP = args.massrange
    input_masses = range(MIN, MAX+STEP, STEP)
elif args.masslist != None:
    # A mass list was provided
    print "Will create mass list according to", args.masslist
    masslist = __import__(args.masslist.replace(".py",""))
    input_masses = masslist.masses
else:
    input_masses = args.mass
# sort masses
input_masses.sort()


from ROOT import *
from array import array
import numpy as np
import CMS_lumi

gROOT.SetBatch(kTRUE);
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)
gStyle.SetTitleFont(42, "XYZ")
gStyle.SetTitleSize(0.06, "XYZ")
gStyle.SetLabelFont(42, "XYZ")
gStyle.SetLabelSize(0.05, "XYZ")
gStyle.SetCanvasBorderMode(0)
gStyle.SetFrameBorderMode(0)
gStyle.SetCanvasColor(kWhite)
gStyle.SetPadTickX(1)
gStyle.SetPadTickY(1)
gStyle.SetPadLeftMargin(0.15)
gStyle.SetPadRightMargin(0.05)
gStyle.SetPadTopMargin(0.06)
gStyle.SetPadBottomMargin(0.14)
gROOT.ForceStyle()

# arrays holding results
masses = array('d')
xs_obs_limits = array('d')
xs_exp_limits = array('d')
masses_exp = array('d')
xs_exp_limits_1sigma = array('d')
xs_exp_limits_1sigma_up = array('d')
xs_exp_limits_2sigma = array('d')
xs_exp_limits_2sigma_up = array('d')

#------------------------------------------------------
if args.run_limits:
    ## for running the limit code
    for mass in input_masses:

      masses.append(float(mass))
      masses_exp.append(float(mass))

      cmd = "./stats " + str(int(mass)) + " " + args.final_state + " | tee stats_" + str(int(mass)) + "_" + args.final_state + ".log"
      print "Running: " + cmd
      proc = subprocess.Popen( cmd, shell=True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT )
      output = proc.communicate()[0]
      if proc.returncode != 0:
        print output
        sys.exit(1)
      #print output

      outputlines = output.split("\n")

      for line in outputlines:
        if re.search("observed bound =", line):
          xs_obs_limits.append(float(line.split()[6]))
        if re.search("median:", line):
          xs_exp_limits.append(float(line.split()[1]))
        if re.search("1 sigma band:", line):
          xs_exp_limits_1sigma.append(float(line.split()[4]))
          xs_exp_limits_1sigma_up.append(float(line.split()[6]))
        if re.search("2 sigma band:", line):
          xs_exp_limits_2sigma.append(float(line.split()[4]))
          xs_exp_limits_2sigma_up.append(float(line.split()[6]))

#------------------------------------------------------
if args.logs_path:
    ## for reading the limit code log files
    for mass in input_masses:

      masses.append(float(mass))
      masses_exp.append(float(mass))

      log_file = open(os.path.join(args.logs_path,"stats_" + str(int(mass)) + "_" + args.final_state + ".log"),'r')
      outputlines = log_file.readlines()
      log_file.close()

      for line in outputlines:
        if re.search("observed bound =", line):
          xs_obs_limits.append(float(line.split()[6]))
        if re.search("median:", line):
          xs_exp_limits.append(float(line.split()[1]))
        if re.search("1 sigma band:", line):
          xs_exp_limits_1sigma.append(float(line.split()[4]))
          xs_exp_limits_1sigma_up.append(float(line.split()[6]))
        if re.search("2 sigma band:", line):
          xs_exp_limits_2sigma.append(float(line.split()[4]))
          xs_exp_limits_2sigma_up.append(float(line.split()[6]))

#------------------------------------------------------

if args.run_limits or args.logs_path:
    if len(xs_exp_limits) > 0:
      for i in range(0,len(masses)):
        masses_exp.append( masses[len(masses)-i-1] )
        xs_exp_limits_1sigma.append( xs_exp_limits_1sigma_up[len(masses)-i-1] )
        xs_exp_limits_2sigma.append( xs_exp_limits_2sigma_up[len(masses)-i-1] )


    print "masses =", masses
    print "xs_obs_limits =", xs_obs_limits
    print "xs_exp_limits =", xs_exp_limits
    print ""
    print "masses_exp =", masses_exp
    print "xs_exp_limits_1sigma =", xs_exp_limits_1sigma
    print "xs_exp_limits_2sigma =", xs_exp_limits_2sigma

########################################################
## RESULTS
if args.read_limits:

    if args.final_state == 'gg' :
        masses = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0])
        xs_obs_limits = array('d', [2.47509, 3.83556, 24.38, 13.3815, 4.18611, 2.17485, 1.77723, 1.75765, 1.58693, 1.87754, 2.35201, 1.96082, 1.8162, 1.61099, 1.48962, 1.53807, 1.30311])
        xs_exp_limits = array('d', [9.20649, 7.58136, 5.5474, 4.84689, 3.943555, 3.34593, 3.052885, 2.730025, 2.4789, 2.155165, 1.74733, 1.624335, 1.55666, 1.40621, 1.271375, 1.162985, 1.09702])

        masses_exp = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0, 1500.0, 1450.0, 1400.0, 1350.0, 1300.0, 1250.0, 1200.0, 1150.0, 1100.0, 1050.0, 1000.0, 950.0, 900.0, 850.0, 800.0, 750.0, 700.0])
        xs_exp_limits_1sigma = array('d', [4.22192756, 3.52222372, 1.9755205, 2.017936, 1.82462012, 1.6315084, 1.47908052, 1.37703238, 1.22126622, 1.06454146, 0.930217212, 0.862816703, 0.808830321, 0.741988258, 0.703393393, 0.655577112, 0.58057537, 1.90200462, 2.0898018, 2.30423766, 2.39050321, 2.49485824, 2.65488995, 2.84378849, 3.89827135, 4.65340453, 4.92151737, 5.73894018, 6.7384512, 8.02012949, 10.824072, 14.9477185, 19.2548896, 25.5987126])
        xs_exp_limits_2sigma = array('d', [2.08252566, 1.61395732, 1.09436785, 1.0272916, 1.01285566, 0.98928764, 0.872577932, 0.750162296, 0.65972131, 0.591024874, 0.596478308, 0.500521867, 0.488227072, 0.451194168, 0.438214457, 0.39438, 0.36439114, 3.07336986, 3.19209823, 3.62205603, 3.97197663, 4.21482762, 4.5095792, 4.76875162, 6.18446269, 6.89642094, 8.20423454, 10.39354763, 11.297656, 12.7953223, 17.249684, 26.436704, 33.8070734, 42.2933726])

        ## with useMCMC=1
        #masses = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0])
        #xs_obs_limits = array('d', [2.81581, 4.41385, 25.0434, 13.328, 4.21163, 2.14324, 1.85214, 1.73309, 1.55195, 1.8677, 3.96351, 2.75234, 1.70195, 1.48608, 1.48886, 1.51766, 1.30182])
        #xs_exp_limits = array('d', [7.69922, 6.30765, 4.439145, 4.46447, 3.771785, 3.23026, 2.695345, 2.5931, 2.25615, 1.97884, 1.61387, 1.592315, 1.51254, 1.35466, 1.25278, 1.15567, 1.08883])

        #masses_exp = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0, 1500.0, 1450.0, 1400.0, 1350.0, 1300.0, 1250.0, 1200.0, 1150.0, 1100.0, 1050.0, 1000.0, 950.0, 900.0, 850.0, 800.0, 750.0, 700.0])
        #xs_exp_limits_1sigma = array('d', [3.55243648, 2.87830975, 1.8549525, 1.95144893, 1.77850087, 1.53419454, 1.41339565, 1.29758292, 1.1320778, 1.0336396, 0.840142525, 0.811133271, 0.784292469, 0.713285804, 0.66180544, 0.626665708, 0.599389008, 1.9674372, 2.09145432, 2.21207266, 2.2727586, 2.57959652, 2.52043303, 2.490795, 3.5418942, 4.0299382, 4.58389424, 5.35980339, 6.15143627, 7.82660841, 9.28852178, 10.85615, 13.8281115, 16.4959224])
        #xs_exp_limits_2sigma = array('d', [1.72525312, 1.4907579, 1.03096625, 1.05323897, 1.03567307, 0.923869401, 0.782900819, 0.72107309, 0.62008722, 0.61636296, 0.56120852, 0.510913074, 0.467982358, 0.443372584, 0.413452578, 0.382227307, 0.363815024, 2.82105408, 3.11418913, 3.32305114, 3.60037628, 4.02972083, 4.04985672, 4.2400136, 5.6881812, 6.6033572, 7.14069114, 8.11573489, 9.85066094, 10.9529718, 13.0989976, 15.8020375, 19.076412, 22.7540648])

        if args.withSyst:
            masses = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0])
            xs_obs_limits = array('d', [7.62311, 12.4745, 38.9157, 25.6973, 11.0892, 5.50526, 4.54439, 4.17164, 3.74474, 4.02983, 4.83448, 4.34305, 3.67513, 3.07661, 2.93706, 2.80332, 2.45866])
            xs_exp_limits = array('d', [24.94525, 20.0278, 14.0946, 12.347, 10.0453, 8.03789, 6.86855, 5.82553, 4.882315, 4.15679, 3.57756, 3.279675, 3.0594, 2.82412, 2.53698, 2.339045, 2.200505])

            masses_exp = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0, 1500.0, 1450.0, 1400.0, 1350.0, 1300.0, 1250.0, 1200.0, 1150.0, 1100.0, 1050.0, 1000.0, 950.0, 900.0, 850.0, 800.0, 750.0, 700.0])
            xs_exp_limits_1sigma = array('d', [10.7054108, 9.43657957, 5.56210608, 5.57949252, 5.03942496, 4.35440348, 3.97999777, 3.35159059, 2.98781557, 2.5710672, 2.18068816, 2.02131102, 1.88272126, 1.72523061, 1.644573, 1.50178367, 1.39247781, 3.13848192, 3.41108138, 3.572418, 4.1243218, 4.44677628, 4.88808508, 5.27124088, 6.35168176, 7.3056482, 8.77989606, 10.5851311, 13.1006722, 16.218792, 21.0629596, 29.3430708, 38.495834, 51.4292588])
            xs_exp_limits_2sigma = array('d', [5.13688698, 4.61854684, 3.08864998, 3.01068592, 2.79561692, 2.68723122, 2.26639224, 1.91550718, 1.69090571, 1.48606008, 1.43427512, 1.29427297, 1.16769702, 1.16542865, 1.0685995, 0.962295846, 0.911886702, 4.15463548, 4.64202673, 4.8416245, 5.4551154, 5.95888584, 6.2361253, 7.15728632, 9.05256072, 10.3057083, 12.2204827, 15.9359835, 18.7913404, 23.3694192, 29.820764, 42.9170458, 54.7254755, 71.3101358])

    elif args.final_state == 'qg' :
        masses = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0])
        xs_obs_limits = array('d', [3.40799, 6.21295, 13.9264, 5.70782, 2.19612, 1.56268, 1.3938, 1.34968, 1.35388, 1.70711, 1.70864, 1.858, 1.17723, 1.11691, 1.09074, 0.990288, 0.791434])
        xs_exp_limits = array('d', [6.07531, 4.85117, 3.819335, 3.370985, 2.86341, 2.42906, 2.1546, 1.918275, 1.7454, 1.56185, 1.33594, 1.177, 1.10291, 0.963783, 0.8798835, 0.838167, 0.800876])

        masses_exp = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0, 1500.0, 1450.0, 1400.0, 1350.0, 1300.0, 1250.0, 1200.0, 1150.0, 1100.0, 1050.0, 1000.0, 950.0, 900.0, 850.0, 800.0, 750.0, 700.0])
        xs_exp_limits_1sigma = array('d', [3.00396305, 2.29708792, 1.77135619, 1.74775538, 1.47126545, 1.30852568, 1.1797683, 1.06988754, 0.948850008, 0.844470164, 0.75833766, 0.665814404, 0.616137539, 0.5804673, 0.530321494, 0.485785988, 0.448397448, 1.33392904, 1.40253118, 1.50348427, 1.5913685, 1.76750534, 1.80399348, 2.084273, 2.55386264, 2.81733008, 3.21888666, 3.7736812, 4.32057082, 5.19457221, 6.52724233, 8.85002585, 11.9687336, 14.7933612])
        xs_exp_limits_2sigma = array('d', [1.47528167, 1.18773248, 1.00775086, 0.982326167, 0.961051456, 0.834978172, 0.717394404, 0.677885726, 0.552057632, 0.551356636, 0.4798778, 0.43095613, 0.405837916, 0.3689899, 0.333775713, 0.304731648, 0.28755978, 2.03792916, 2.12631966, 2.23035829, 2.622506, 2.72506598, 2.9131448, 3.3603518, 4.01153776, 4.58135816, 5.18838731, 6.25111164, 6.86364771, 8.16430152, 10.237121, 15.143471, 20.5361704, 25.318511])

        if args.withSyst:
            masses = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0])
            xs_obs_limits = array('d', [11.7714, 16.9776, 22.0275, 11.6582, 5.35112, 3.55859, 2.85859, 2.75599, 2.60531, 2.94131, 3.25826, 2.62847, 2.16147, 1.91004, 1.7581, 1.6015, 1.37303])
            xs_exp_limits = array('d', [16.26235, 12.7514, 9.2586, 7.807355, 6.31827, 5.11768, 4.27029, 3.65319, 3.03223, 2.53214, 2.19563, 2.019015, 1.87499, 1.70662, 1.56985, 1.48363, 1.39514])

            masses_exp = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0, 1500.0, 1450.0, 1400.0, 1350.0, 1300.0, 1250.0, 1200.0, 1150.0, 1100.0, 1050.0, 1000.0, 950.0, 900.0, 850.0, 800.0, 750.0, 700.0])
            xs_exp_limits_1sigma = array('d', [8.57326791, 6.29133037, 4.81116696, 4.56481338, 3.88110205, 3.28715049, 2.81806512, 2.3694125, 2.0318288, 1.69524353, 1.463313, 1.3562918, 1.23118616, 1.1569014, 1.07622912, 1.00203242, 0.947418968, 1.94466496, 2.10662002, 2.27367728, 2.4710025, 2.65119008, 2.86734375, 3.163109, 3.71789763, 4.37928624, 5.2822685, 6.36219588, 7.56951872, 9.35415135, 11.7946748, 16.6866189, 23.5578399, 32.2253054])
            xs_exp_limits_2sigma = array('d', [4.23642838, 3.35230338, 2.6614277, 2.60141767, 2.5135218, 2.17023188, 1.80342668, 1.500109, 1.31144048, 1.16621604, 1.037869, 0.91741915, 0.859661848, 0.81254073, 0.75111756, 0.686176256, 0.645147192, 2.504322, 2.77958765, 3.05711048, 3.30261355, 3.55924096, 3.8603353, 4.305882, 5.25932838, 6.11021272, 7.4080065, 8.93614776, 10.8428404, 13.502193, 16.7983999, 23.6200834, 33.1814697, 43.738617])

    elif args.final_state == 'qq' :
        masses = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0])
        xs_obs_limits = array('d', [3.97496, 8.63273, 6.8204, 3.07626, 1.67548, 1.25714, 1.15667, 1.11332, 1.2765, 1.8935, 1.70738, 1.24373, 0.932713, 0.874972, 0.894555, 0.762036, 0.531577])
        xs_exp_limits = array('d', [4.552785, 3.54086, 3.20133, 2.71352, 2.214, 1.897145, 1.70106, 1.561165, 1.380675, 1.166545, 1.02438, 0.965802, 0.862712, 0.786767, 0.7418015, 0.669594, 0.607163])

        masses_exp = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0, 1500.0, 1450.0, 1400.0, 1350.0, 1300.0, 1250.0, 1200.0, 1150.0, 1100.0, 1050.0, 1000.0, 950.0, 900.0, 850.0, 800.0, 750.0, 700.0])
        xs_exp_limits_1sigma = array('d', [2.5550011, 1.82563187, 1.78552939, 1.61348244, 1.3332637, 1.19804683, 1.06281009, 0.929739499, 0.844479872, 0.764291033, 0.643902062, 0.58532178, 0.54576966, 0.496991908, 0.452589784, 0.422559728, 0.385806298, 0.944544732, 0.987172704, 1.11505353, 1.18185774, 1.31482571, 1.4085597, 1.56003362, 1.80501629, 2.08968565, 2.44658417, 2.71382277, 3.12875609, 3.55370138, 4.41192076, 5.73288905, 7.21221147, 8.86276339])
        xs_exp_limits_2sigma = array('d', [1.53013283, 1.09065427, 1.09091265, 0.97539764, 0.933030366, 0.791534445, 0.72910934, 0.613181656, 0.508796916, 0.483979013, 0.435465392, 0.40714065, 0.379658811, 0.349976614, 0.297233336, 0.27033796, 0.262967828, 1.39955342, 1.47423776, 1.63252919, 1.74485518, 1.97941674, 2.0193863, 2.28595402, 2.59595822, 3.25479824, 3.7805289, 4.18913156, 4.70605081, 5.83567828, 6.74364516, 8.84648885, 11.6105067, 14.2713312])

        if args.withSyst:
            masses = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0])
            xs_obs_limits = array('d', [9.90295, 14.5168, 10.3987, 5.68074, 3.33093, 2.43718, 1.97698, 1.94017, 2.02482, 2.4409, 2.30197, 1.73818, 1.34864, 1.23991, 1.19888, 1.06035, 0.868413])
            xs_exp_limits = array('d', [11.3328, 8.31654, 6.11933, 4.971535, 4.14249, 3.44584, 2.86092, 2.385205, 2.000665, 1.68192, 1.5038, 1.38957, 1.25428, 1.127585, 1.05383, 1.00789, 0.9220155])

            masses_exp = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0, 1500.0, 1450.0, 1400.0, 1350.0, 1300.0, 1250.0, 1200.0, 1150.0, 1100.0, 1050.0, 1000.0, 950.0, 900.0, 850.0, 800.0, 750.0, 700.0])
            xs_exp_limits_1sigma = array('d', [7.0428176, 4.74435615, 4.08169282, 3.5135587, 2.90090892, 2.43968133, 2.04613366, 1.7065494, 1.43213525, 1.19363256, 1.06613648, 0.947751163, 0.867407716, 0.814140565, 0.754859829, 0.70456552, 0.671453753, 1.28959074, 1.3877516, 1.50125125, 1.55922475, 1.77772776, 1.9435408, 2.12567196, 2.42203896, 2.89060783, 3.3641405, 4.17541306, 4.94155918, 5.81280244, 6.9950286, 9.10725102, 12.9761905, 17.6836846])
            xs_exp_limits_2sigma = array('d', [4.32860814, 2.90031815, 2.71833718, 2.31717825, 2.130137, 1.71954162, 1.44481906, 1.19102145, 1.03948668, 0.86933932, 0.766403336, 0.67171837, 0.62722948, 0.594010595, 0.537929382, 0.51665864, 0.483697909, 1.71190317, 1.81709, 1.99281756, 2.13448985, 2.3650806, 2.55049911, 2.80627188, 3.42978688, 4.03166177, 4.7879625, 5.97480324, 6.51966432, 8.08329992, 9.62284665, 13.0393877, 18.588047, 24.8679442])

    # the following commented out portion for some reason causes problems in CMSSW_7_0_9 but works fine with later CMSSW release cycles, e.g. 74X
    # since non-essential, keeping it commented out
    ## new temporary arrays
    #all_masses = copy.deepcopy(masses)
    #all_xs_obs_limits = copy.deepcopy(xs_obs_limits)
    #all_xs_exp_limits = copy.deepcopy(xs_exp_limits)
    #all_masses_exp = copy.deepcopy(masses_exp)
    #all_xs_exp_limits_1sigma = copy.deepcopy(xs_exp_limits_1sigma)
    #all_xs_exp_limits_2sigma = copy.deepcopy(xs_exp_limits_2sigma)

    #all_masses_array = np.array(all_masses)
    #all_masses_exp_array = np.array(all_masses_exp)
    #indices = []
    #indices_exp = []

    ## search for indices of input_masses
    #for mass in input_masses:
        #where = np.where(all_masses_array==mass)[0]
        #if len(where) == 0:
            #print "** WARNING: ** Cannot find results for m =", int(mass), "GeV in the provided results file. Skipping this mass point."
        #indices.extend( where )
        #if len(all_masses_exp) > 0:
            #where = np.where(all_masses_exp_array==mass)[0]
            #if len(where) == 0:
                #print "** WARNING: ** Cannot find results for m =", int(mass), "GeV in the provided results file. Skipping this mass point."
            #indices_exp.extend( where )

    ## sort indices
    #indices.sort()
    #indices_exp.sort()

    ## reset old arrays
    #masses = array('d')
    #xs_obs_limits = array('d')
    #xs_exp_limits = array('d')
    #masses_exp = array('d')
    #xs_exp_limits_1sigma = array('d')
    #xs_exp_limits_2sigma = array('d')

    #for i in indices:
        #masses.append( all_masses[i] )
        #xs_obs_limits.append( all_xs_obs_limits[i] )
        #if len(all_masses_exp) > 0: xs_exp_limits.append( all_xs_exp_limits[i] )
    #for i in indices_exp:
        #masses_exp.append( all_masses_exp[i] )
        #xs_exp_limits_1sigma.append( all_xs_exp_limits_1sigma[i] )
        #xs_exp_limits_2sigma.append( all_xs_exp_limits_2sigma[i] )

    #print masses
    #print xs_obs_limits
    #print xs_exp_limits
    #print masses_exp
    #print xs_exp_limits_1sigma
    #print xs_exp_limits_2sigma

# rescale limits to the correct integrated luminosity
scaleFactor = 1866./1914.

xs_obs_limits = array('d',(np.array(xs_obs_limits.tolist())*scaleFactor).tolist())
xs_exp_limits = array('d',(np.array(xs_exp_limits.tolist())*scaleFactor).tolist())

xs_exp_limits_1sigma = array('d',(np.array(xs_exp_limits_1sigma.tolist())*scaleFactor).tolist())
xs_exp_limits_2sigma = array('d',(np.array(xs_exp_limits_2sigma.tolist())*scaleFactor).tolist())

##
########################################################

# theory curves: gg
massesS8 = array('d', [1000.0,1100.0,1200.0,1300.0,1400.0,1500.0,1600.0,1700.0,1800.0,1900.0,2000.0,2100.0,2200.0,2300.0,2400.0,2500.0,2600.0,2700.0,2800.0,2900.0,3000.0,3100.0,3200.0,3300.0,3400.0,3500.0,3600.0,3700.0,3800.0,3900.0,4000.0,4100.0,4200.0,4300.0,4400.0,4500.0,4600.0,4700.0,4800.0,4900.0,5000.0,5100.0,5200.0,5300.0,5400.0,5500.0,5600.0,5700.0,5800.0,5900.0,6000.0])
xsS8 = array('d', [5.15E+02,2.93E+02,1.73E+02,1.11E+02,6.68E+01,4.29E+01,2.86E+01,1.90E+01,1.30E+01,8.71E+00,6.07E+00,4.32E+00,2.99E+00,2.14E+00,1.53E+00,1.09E+00,8.10E-01,5.83E-01,4.38E-01,3.25E-01,2.43E-01,1.78E-01,1.37E-01,1.03E-01,7.66E-02,5.76E-02,4.46E-02,3.42E-02,2.60E-02,1.94E-02,1.50E-02,1.20E-02,9.12E-03,6.99E-03,5.47E-03,4.19E-03,3.21E-03,2.53E-03,1.90E-03,1.50E-03,1.18E-03,9.13E-04,7.07E-04,5.60E-04,4.35E-04,3.36E-04,2.59E-04,2.09E-04,1.59E-04,1.21E-04,9.38E-05])

xs_maxS8 = 2e+03
idxS8 = 0

for i, xs in enumerate(xsS8):
  if xs < xs_maxS8:
    idxS8 = i
    break

#------------------------------------------------------
# theory curves: qg
massesQstar = array('d', [500.0,550.0,600.0,650.0,700.0,750.0,800.0,850.0,900.0,950.0,1000.0,1100.0,1200.0,1300.0,1400.0,1500.0,1600.0,1700.0,1800.0,1900.0,2000.0,2100.0,2200.0,2300.0,2400.0,2500.0,2600.0,2700.0,2800.0,2900.0,3000.0,3100.0,3200.0,3300.0,3400.0,3500.0,3600.0,3700.0,3800.0,3900.0,4000.0,4100.0,4200.0,4300.0,4400.0,4500.0,4600.0,4700.0,4800.0,4900.0,5000.0,5100.0,5200.0,5300.0,5400.0,5500.0,5600.0,5700.0,5800.0,5900.0,6000.0,6100.0,6200.0,6300.0,6400.0,6500.0,6600.0,6700.0,6800.0,6900.0,7000.0,7100.0,7200.0,7300.0,7400.0,7500.0,7600.0,7700.0,7800.0,7900.0,8000.0,8100.0,8200.0,8300.0,8400.0,8500.0,8600.0,8700.0,8800.0,8900.0,9000.0])
xsQstar = array('d', [0.7483E+04,0.5185E+04,0.3676E+04,0.2664E+04,0.1963E+04,0.1468E+04,0.1114E+04,0.8546E+03,0.6628E+03,0.5190E+03,0.4101E+03,0.2620E+03,0.1721E+03,0.1157E+03,0.7934E+02,0.5540E+02,0.3928E+02,0.2823E+02,0.2054E+02,0.1510E+02,0.1121E+02,0.8390E+01,0.6328E+01,0.4807E+01,0.3674E+01,0.2824E+01,0.2182E+01,0.1694E+01,0.1320E+01,0.1033E+01,0.8116E+00,0.6395E+00,0.5054E+00,0.4006E+00,0.3182E+00,0.2534E+00,0.2022E+00,0.1616E+00,0.1294E+00,0.1038E+00,0.8333E-01,0.6700E-01,0.5392E-01,0.4344E-01,0.3503E-01,0.2827E-01,0.2283E-01,0.1844E-01,0.1490E-01,0.1205E-01,0.9743E-02,0.7880E-02,0.6373E-02,0.5155E-02,0.4169E-02,0.3371E-02,0.2725E-02,0.2202E-02,0.1779E-02,0.1437E-02,0.1159E-02,0.9353E-03,0.7541E-03,0.6076E-03,0.4891E-03,0.3935E-03,0.3164E-03,0.2541E-03,0.2039E-03,0.1635E-03,0.1310E-03,0.1049E-03,0.8385E-04,0.6699E-04,0.5347E-04,0.4264E-04,0.3397E-04,0.2704E-04,0.2151E-04,0.1709E-04,0.1357E-04,0.1077E-04,0.8544E-05,0.6773E-05,0.5367E-05,0.4251E-05,0.3367E-05,0.2666E-05,0.2112E-05,0.1673E-05,0.1326E-05])

xs_maxQstar = 2e+03
idxQstar = 0

for i, xs in enumerate(xsQstar):
  if xs < xs_maxQstar:
    idxQstar = i
    break

#------------------------------------------------------
# theory curves: qq
massesTh = array('d', [500.0,550.0,600.0,650.0,700.0,750.0,800.0,850.0,900.0,950.0,1000.0,1100.0,1200.0,1300.0,1400.0,1500.0,1600.0,1700.0,1800.0,1900.0,2000.0,2100.0,2200.0,2300.0,2400.0,2500.0,2600.0,2700.0,2800.0,2900.0,3000.0,3100.0,3200.0,3300.0,3400.0,3500.0,3600.0,3700.0,3800.0,3900.0,4000.0,4100.0,4200.0,4300.0,4400.0,4500.0,4600.0,4700.0,4800.0,4900.0,5000.0,5100.0,5200.0,5300.0,5400.0,5500.0,5600.0,5700.0,5800.0,5900.0,6000.0,6100.0,6200.0,6300.0,6400.0,6500.0,6600.0,6700.0,6800.0,6900.0,7000.0,7100.0,7200.0,7300.0,7400.0,7500.0,7600.0,7700.0,7800.0,7900.0,8000.0,8100.0,8200.0,8300.0,8400.0,8500.0,8600.0,8700.0,8800.0,8900.0,9000.0])

xsAxi = array('d', [0.2492E+04,0.1760E+04,0.1274E+04,0.9436E+03,0.7115E+03,0.5448E+03,0.4232E+03,0.3325E+03,0.2641E+03,0.2117E+03,0.1712E+03,0.1144E+03,0.7846E+02,0.5497E+02,0.3921E+02,0.2842E+02,0.2086E+02,0.1550E+02,0.1163E+02,0.8802E+01,0.6713E+01,0.5157E+01,0.3984E+01,0.3095E+01,0.2416E+01,0.1894E+01,0.1491E+01,0.1177E+01,0.9328E+00,0.7410E+00,0.5901E+00,0.4710E+00,0.3767E+00,0.3019E+00,0.2423E+00,0.1947E+00,0.1567E+00,0.1262E+00,0.1017E+00,0.8207E-01,0.6626E-01,0.5352E-01,0.4324E-01,0.3494E-01,0.2824E-01,0.2283E-01,0.1845E-01,0.1490E-01,0.1203E-01,0.9711E-02,0.7833E-02,0.6314E-02,0.5085E-02,0.4091E-02,0.3288E-02,0.2639E-02,0.2115E-02,0.1693E-02,0.1353E-02,0.1079E-02,0.8594E-03,0.6830E-03,0.5417E-03,0.4286E-03,0.3383E-03,0.2664E-03,0.2092E-03,0.1638E-03,0.1279E-03,0.9958E-04,0.7727E-04,0.5977E-04,0.4606E-04,0.3537E-04,0.2707E-04,0.2064E-04,0.1567E-04,0.1185E-04,0.8929E-05,0.6698E-05,0.5005E-05,0.3724E-05,0.2760E-05,0.2037E-05,0.1498E-05,0.1097E-05,0.8002E-06,0.5820E-06,0.4218E-06,0.3046E-06,0.2195E-06])
xsDiquark = array('d', [0.4952E+03,0.3745E+03,0.2888E+03,0.2268E+03,0.1808E+03,0.1460E+03,0.1193E+03,0.9843E+02,0.8198E+02,0.6884E+02,0.5824E+02,0.4250E+02,0.3172E+02,0.2411E+02,0.1862E+02,0.1457E+02,0.1153E+02,0.9211E+01,0.7419E+01,0.6019E+01,0.4912E+01,0.4031E+01,0.3323E+01,0.2750E+01,0.2284E+01,0.1903E+01,0.1590E+01,0.1331E+01,0.1117E+01,0.9386E+00,0.7900E+00,0.6658E+00,0.5618E+00,0.4745E+00,0.4010E+00,0.3391E+00,0.2869E+00,0.2428E+00,0.2055E+00,0.1740E+00,0.1473E+00,0.1246E+00,0.1055E+00,0.8922E-01,0.7544E-01,0.6376E-01,0.5385E-01,0.4546E-01,0.3834E-01,0.3231E-01,0.2720E-01,0.2288E-01,0.1922E-01,0.1613E-01,0.1352E-01,0.1132E-01,0.9463E-02,0.7900E-02,0.6584E-02,0.5479E-02,0.4551E-02,0.3774E-02,0.3124E-02,0.2581E-02,0.2128E-02,0.1750E-02,0.1437E-02,0.1177E-02,0.9612E-03,0.7833E-03,0.6366E-03,0.5160E-03,0.4170E-03,0.3360E-03,0.2700E-03,0.2162E-03,0.1725E-03,0.1372E-03,0.1087E-03,0.8577E-04,0.6742E-04,0.5278E-04,0.4114E-04,0.3192E-04,0.2465E-04,0.1894E-04,0.1448E-04,0.1101E-04,0.8322E-05,0.6253E-05,0.4670E-05])
xsWprime = array('d', [0.1063E+03,0.7731E+02,0.5744E+02,0.4351E+02,0.3348E+02,0.2611E+02,0.2062E+02,0.1645E+02,0.1325E+02,0.1076E+02,0.8811E+01,0.6024E+01,0.4216E+01,0.3010E+01,0.2185E+01,0.1610E+01,0.1200E+01,0.9043E+00,0.6875E+00,0.5271E+00,0.4067E+00,0.3158E+00,0.2464E+00,0.1932E+00,0.1521E+00,0.1201E+00,0.9512E-01,0.7554E-01,0.6012E-01,0.4792E-01,0.3827E-01,0.3059E-01,0.2448E-01,0.1960E-01,0.1571E-01,0.1259E-01,0.1009E-01,0.8090E-02,0.6483E-02,0.5193E-02,0.4158E-02,0.3327E-02,0.2660E-02,0.2125E-02,0.1695E-02,0.1351E-02,0.1075E-02,0.8546E-03,0.6781E-03,0.5372E-03,0.4248E-03,0.3353E-03,0.2642E-03,0.2077E-03,0.1629E-03,0.1275E-03,0.9957E-04,0.7757E-04,0.6027E-04,0.4670E-04,0.3610E-04,0.2783E-04,0.2140E-04,0.1641E-04,0.1254E-04,0.9561E-05,0.7269E-05,0.5510E-05,0.4167E-05,0.3143E-05,0.2364E-05,0.1774E-05,0.1329E-05,0.9931E-06,0.7411E-06,0.5523E-06,0.4108E-06,0.3055E-06,0.2271E-06,0.1687E-06,0.1254E-06,0.9327E-07,0.6945E-07,0.5177E-07,0.3863E-07,0.2888E-07,0.2162E-07,0.1622E-07,0.1218E-07,0.9156E-08,0.6893E-08])
xsZprime = array('d', [0.6574E+02,0.4728E+02,0.3476E+02,0.2609E+02,0.1990E+02,0.1540E+02,0.1207E+02,0.9565E+01,0.7653E+01,0.6178E+01,0.5027E+01,0.3398E+01,0.2353E+01,0.1663E+01,0.1196E+01,0.8729E+00,0.6450E+00,0.4822E+00,0.3638E+00,0.2769E+00,0.2123E+00,0.1639E+00,0.1272E+00,0.9933E-01,0.7789E-01,0.6134E-01,0.4848E-01,0.3845E-01,0.3059E-01,0.2440E-01,0.1952E-01,0.1564E-01,0.1256E-01,0.1010E-01,0.8142E-02,0.6570E-02,0.5307E-02,0.4292E-02,0.3473E-02,0.2813E-02,0.2280E-02,0.1848E-02,0.1499E-02,0.1216E-02,0.9864E-03,0.8002E-03,0.6490E-03,0.5262E-03,0.4264E-03,0.3453E-03,0.2795E-03,0.2260E-03,0.1826E-03,0.1474E-03,0.1188E-03,0.9566E-04,0.7690E-04,0.6173E-04,0.4947E-04,0.3957E-04,0.3159E-04,0.2516E-04,0.2001E-04,0.1587E-04,0.1255E-04,0.9906E-05,0.7795E-05,0.6116E-05,0.4785E-05,0.3731E-05,0.2900E-05,0.2247E-05,0.1734E-05,0.1334E-05,0.1022E-05,0.7804E-06,0.5932E-06,0.4492E-06,0.3388E-06,0.2544E-06,0.1903E-06,0.1417E-06,0.1051E-06,0.7764E-07,0.5711E-07,0.4186E-07,0.3055E-07,0.2223E-07,0.1612E-07,0.1164E-07,0.8394E-08])
xsRSG = array('d', [0.2534E+03,0.1632E+03,0.1085E+03,0.7421E+02,0.5193E+02,0.3707E+02,0.2695E+02,0.1990E+02,0.1490E+02,0.1130E+02,0.8673E+01,0.5261E+01,0.3306E+01,0.2139E+01,0.1420E+01,0.9639E+00,0.6666E+00,0.4691E+00,0.3349E+00,0.2424E+00,0.1774E+00,0.1312E+00,0.9793E-01,0.7373E-01,0.5591E-01,0.4269E-01,0.3279E-01,0.2533E-01,0.1966E-01,0.1532E-01,0.1200E-01,0.9422E-02,0.7426E-02,0.5870E-02,0.4652E-02,0.3696E-02,0.2942E-02,0.2346E-02,0.1874E-02,0.1499E-02,0.1200E-02,0.9625E-03,0.7724E-03,0.6202E-03,0.4983E-03,0.4005E-03,0.3220E-03,0.2589E-03,0.2082E-03,0.1673E-03,0.1345E-03,0.1080E-03,0.8669E-04,0.6953E-04,0.5573E-04,0.4462E-04,0.3568E-04,0.2850E-04,0.2273E-04,0.1810E-04,0.1439E-04,0.1142E-04,0.9042E-05,0.7147E-05,0.5635E-05,0.4433E-05,0.3479E-05,0.2722E-05,0.2125E-05,0.1653E-05,0.1282E-05,0.9914E-06,0.7639E-06,0.5866E-06,0.4489E-06,0.3423E-06,0.2599E-06,0.1966E-06,0.1481E-06,0.1112E-06,0.8308E-07,0.6183E-07,0.4584E-07,0.3384E-07,0.2489E-07,0.1823E-07,0.1330E-07,0.9672E-08,0.7008E-08,0.5059E-08,0.3645E-08])

massesRSGgg = array('d')
xsRSGgg = array('d')
fractionRSGgg = [0.27903,0.27503,0.27121,0.26705,0.26295,0.25870,0.25433,0.25003,0.24566,0.24125,0.23673,0.22778,0.21881,0.20990,0.20108,0.19242,0.18393,0.17570,0.16771,0.16002,0.15260,0.14551,0.13870,0.13223,0.12604,0.12017,0.11459,0.10929,0.10428,0.09951,0.09501,0.09074,0.08670,0.08287,0.07924,0.07582,0.07257,0.06949,0.06658,0.06381,0.06120,0.05872,0.05636,0.05413,0.05201,0.05001,0.04810,0.04629,0.04457,0.04295,0.04140,0.03993,0.03854,0.03721,0.03596,0.03478,0.03364,0.03258,0.03157,0.03061,0.02971,0.02885,0.02805,0.02729,0.02657,0.02589,0.02526,0.02466,0.02411,0.02358,0.02309,0.02264,0.02221,0.02182,0.02145,0.02111,0.02079,0.02049,0.02021,0.01994,0.01969,0.01944,0.01920,0.01895,0.01870,0.01844,0.01816,0.01785,0.01751,0.01713,0.01670]

for i in range(0,len(fractionRSGgg)):
  massesRSGgg.append(massesTh[i])
  xsRSGgg.append( xsRSG[i]*fractionRSGgg[i] )


xs_maxQQ = 2e+03
idxAxi = 0
idxDiquark = 0

for i, xs in enumerate(xsAxi):
  if xs < xs_maxQQ:
    idxAxi = i
    break

for i, xs in enumerate(xsDiquark):
  if xs < xs_maxQQ:
    idxDiquark = i
    break

#------------------------------------------------------
if args.useTeV:
  masses = array('d',(np.array(masses.tolist())/1000.).tolist())
  masses_exp = array('d',(np.array(masses_exp.tolist())/1000.).tolist())
  massesS8 = array('d',(np.array(massesS8.tolist())/1000.).tolist())
  massesQstar = array('d',(np.array(massesQstar.tolist())/1000.).tolist())
  massesTh = array('d',(np.array(massesTh.tolist())/1000.).tolist())
  massesRSGgg = array('d',(np.array(massesRSGgg.tolist())/1000.).tolist())

# theory curves: gg
graph_xsS8 = TGraph(len(massesS8[idxS8:-1]),massesS8[idxS8:-1],xsS8[idxS8:-1])
graph_xsS8.SetLineWidth(3)
graph_xsS8.SetLineStyle(6)
graph_xsS8.SetLineColor(6)

# theory curves: qg
graph_xsQstar = TGraph(len(massesQstar[idxQstar:-1]),massesQstar[idxQstar:-1],xsQstar[idxQstar:-1])
graph_xsQstar.SetLineWidth(3)
graph_xsQstar.SetLineStyle(2)
graph_xsQstar.SetLineColor(1)

# theory curves: qq
graph_xsAxi = TGraph(len(massesTh[idxAxi:-1]),massesTh[idxAxi:-1],xsAxi[idxAxi:-1])
graph_xsAxi.SetLineWidth(3)
graph_xsAxi.SetLineStyle(3)
graph_xsAxi.SetLineColor(63)

graph_xsDiquark = TGraph(len(massesTh[idxDiquark:-1]),massesTh[idxDiquark:-1],xsDiquark[idxDiquark:-1])
graph_xsDiquark.SetLineWidth(3)
graph_xsDiquark.SetLineStyle(9)
graph_xsDiquark.SetLineColor(42)

graph_xsWprime = TGraph(len(massesTh),massesTh,xsWprime)
graph_xsWprime.SetLineWidth(3)
graph_xsWprime.SetLineStyle(7)
graph_xsWprime.SetLineColor(46)

graph_xsZprime = TGraph(len(massesTh),massesTh,xsZprime)
graph_xsZprime.SetLineWidth(3)
graph_xsZprime.SetLineStyle(5)
graph_xsZprime.SetLineColor(38)

graph_xsRSG = TGraph(len(massesTh),massesTh,xsRSG)
graph_xsRSG.SetLineWidth(3)
graph_xsRSG.SetLineStyle(4)
graph_xsRSG.SetLineColor(30)

graph_xsRSGgg = TGraph(len(massesRSGgg),massesRSGgg,xsRSGgg)
graph_xsRSGgg.SetLineWidth(3)
graph_xsRSGgg.SetLineStyle(8)
graph_xsRSGgg.SetLineColor(32)

#------------------------------------------------------
graph_exp_2sigma = TGraph(len(masses_exp),masses_exp,xs_exp_limits_2sigma)
graph_exp_2sigma.SetFillColor(kYellow)
graph_exp_2sigma.GetXaxis().SetTitle(args.final_state + " resonance mass [" + ("TeV" if args.useTeV else "GeV") + "]")
graph_exp_2sigma.GetYaxis().SetTitle("#sigma #it{B} #it{A} [pb]")
graph_exp_2sigma.GetYaxis().SetTitleOffset(1.1)
graph_exp_2sigma.GetYaxis().SetRangeUser(1e-01,1e+04)
#graph_exp_2sigma.GetXaxis().SetNdivisions(1005)

graph_exp_1sigma = TGraph(len(masses_exp),masses_exp,xs_exp_limits_1sigma)
graph_exp_1sigma.SetFillColor(kGreen+1)

graph_exp = TGraph(len(masses),masses,xs_exp_limits)
#graph_exp.SetMarkerStyle(24)
graph_exp.SetLineWidth(3)
graph_exp.SetLineStyle(4)
graph_exp.SetLineColor(4)

graph_obs = TGraph(len(masses),masses,xs_obs_limits)
graph_obs.SetMarkerStyle(20)
graph_obs.SetLineWidth(3)
#graph_obs.SetLineStyle(1)
graph_obs.SetLineColor(1)


c = TCanvas("c", "",800,800)
c.cd()

graph_exp_2sigma.Draw("AF")
graph_exp_1sigma.Draw("F")
graph_exp.Draw("L")
graph_obs.Draw("LP")

if args.final_state == 'gg' :
    #graph_xsS8.Draw("L")
    graph_xsRSGgg.Draw("L")
    pass
elif args.final_state == 'qg' :
    graph_xsQstar.Draw("L")
elif args.final_state == 'qq' :
    graph_xsAxi.Draw("L")
    graph_xsDiquark.Draw("L")
    graph_xsWprime.Draw("L")
    graph_xsZprime.Draw("L")


legend = TLegend(.59,.50,.94,.70)
if args.final_state == 'qg' :
    legend = TLegend(.59,.70,.94,.90)
if args.final_state == 'qq' :
    legend = TLegend(.59,.67,.94,.87)
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetFillStyle(0)
legend.SetTextFont(42)
legend.SetTextSize(0.035)
legend.SetMargin(0.20)
legend.SetHeader('95% CL upper limits')
legend.AddEntry(graph_obs,"Observed","lp")
legend.AddEntry(graph_exp,"Expected","lp")
legend.AddEntry(graph_exp_1sigma,"#pm 1 std. deviation","F")
legend.AddEntry(graph_exp_2sigma,"#pm 2 std. deviation","F")
legend.Draw()

legendTh = TLegend(.50,.80,.85,.84)
if args.final_state == 'qg' :
    legendTh = TLegend(.59,.52,.94,.60)
elif args.final_state == 'qq' :
    legendTh = TLegend(.19,.17,.54,.31)
legendTh.SetBorderSize(0)
legendTh.SetFillColor(0)
legendTh.SetFillStyle(0)
legendTh.SetTextFont(42)
legendTh.SetTextSize(0.035)
legendTh.SetMargin(0.20)
if args.final_state == 'gg' :
    #legendTh.AddEntry(graph_xsS8,"Color-octet scalar","l")
    legendTh.AddEntry(graph_xsRSGgg,"RS graviton (gg#rightarrowG#rightarrowgg)","l")
    pass
elif args.final_state == 'qg' :
    legendTh.AddEntry(graph_xsQstar,"Excited quark","l")
elif args.final_state == 'qq' :
    legendTh.AddEntry(graph_xsAxi,"Axigluon/coloron","l")
    legendTh.AddEntry(graph_xsDiquark,"Scalar diquark","l")
    legendTh.AddEntry(graph_xsWprime,"W'","l")
    legendTh.AddEntry(graph_xsZprime,"Z'","l")
legendTh.Draw()

#draw the lumi text on the canvas
CMS_lumi.extraText = args.extraText
CMS_lumi.lumi_sqrtS = args.lumi_sqrtS # used with iPeriod = 0 (free form)
iPos = 11
if( iPos==0 ): CMS_lumi.relPosX = 0.15
iPeriod = 0

CMS_lumi.CMS_lumi(c, iPeriod, iPos)

gPad.RedrawAxis()

c.SetLogy()
fileName = 'xs_limit_DijetLimitCode_%s.%s'%(args.final_state + ('_NoSyst' if not args.withSyst else '') + ( ('_' + args.postfix) if args.postfix != '' else '' ), args.fileFormat.lower())
c.SaveAs(fileName)
print "Plot saved to '%s'"%(fileName)
