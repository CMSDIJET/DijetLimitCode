#!/usr/bin/env python

import string, re
from ROOT import *
from array import array
import numpy as np
import CMS_lumi


gROOT.SetBatch(kTRUE);
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)
gStyle.SetTitleFont(42, "XYZ")
gStyle.SetTitleSize(0.06, "XYZ")
gStyle.SetLabelFont(42, "XYZ")
gStyle.SetLabelSize(0.05, "XYZ")
gStyle.SetCanvasBorderMode(0)
gStyle.SetFrameBorderMode(0)
gStyle.SetCanvasColor(kWhite)
gStyle.SetPadTickX(1)
gStyle.SetPadTickY(1)
gStyle.SetPadLeftMargin(0.15)
gStyle.SetPadRightMargin(0.05)
gStyle.SetPadTopMargin(0.06)
gStyle.SetPadBottomMargin(0.14)
gROOT.ForceStyle()

useTeV = True
#useTeV = False

masses = array('d', [700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0])
xs_stat = array('d', [2.47509, 3.83556, 24.38, 13.3815, 4.18611, 2.17485, 1.77723, 1.75765, 1.58693, 1.87754, 2.35201, 1.96082, 1.8162, 1.61099, 1.48962, 1.53807, 1.30311])

xs_sys_all = array('d', [7.62311, 12.4745, 38.9157, 25.6973, 11.0892, 5.50526, 4.54439, 4.17164, 3.74474, 4.02983, 4.83448, 4.34305, 3.67513, 3.07661, 2.93706, 2.80332, 2.45866])
xs_sys_lumi = array('d', [2.84053, 4.3835, 25.1666, 13.4295, 4.22637, 2.14597, 1.86311, 1.73077, 1.54107, 1.87591, 3.97207, 2.75339, 1.69937, 1.48176, 1.48648, 1.51407, 1.29853])
xs_sys_jes = array('d', [2.81263, 4.50323, 24.9085, 13.8465, 4.32404, 2.14957, 1.85005, 1.7302, 1.56094, 1.89958, 3.91676, 2.8202, 1.72763, 1.49258, 1.49974, 1.50932, 1.31075])
xs_sys_jer = array('d', [2.79365, 4.61496, 25.5202, 13.5983, 4.24688, 2.17101, 1.83442, 1.72637, 1.53159, 1.88007, 3.93073, 2.77806, 1.7356, 1.49967, 1.5002, 1.52937, 1.30238])
xs_sys_trig = array('d', [4.41636, 5.24176, 25.3345, 13.5814, 4.3644, 2.18056, 1.87701, 1.74779, 1.53964, 1.8942, 3.98694, 2.75276, 1.70617, 1.4898, 1.50215, 1.51291, 1.29454])
xs_sys_allexbkg = array('d', [4.51965, 5.64179, 26.398, 14.6124, 4.54198, 2.25662, 1.90079, 1.76636, 1.58605, 1.90022, 3.91246, 2.86253, 1.78668, 1.53774, 1.52336, 1.52615, 1.32586])
xs_sys_bkg = array('d', [7.99632, 11.6233, 35.9242, 20.5528, 10.3065, 5.47003, 4.4636, 3.90344, 3.34618, 3.53204, 5.33044, 4.01728, 3.01208, 2.96939, 2.73101, 2.65312, 2.22769])


r_all = array('d')
r_lumi  = array('d')
r_jes  = array('d')
r_jer  = array('d')
r_trig  = array('d')
r_allexbkg  = array('d')
r_bkg  = array('d')

for i in range(0,len(xs_stat)):
  r_all.append( xs_sys_all[i] / xs_stat[i] )
  r_lumi.append( xs_sys_lumi[i] / xs_stat[i] )
  r_jes.append( xs_sys_jes[i] / xs_stat[i] )
  r_jer.append( xs_sys_jer[i] / xs_stat[i] )
  r_trig.append( xs_sys_trig[i] / xs_stat[i] )
  r_allexbkg.append( xs_sys_allexbkg[i] / xs_stat[i] )
  r_bkg.append( xs_sys_bkg[i] / xs_stat[i] )

if useTeV:
  masses = array('d',(np.array(masses.tolist())/1000.).tolist())

g_all = TGraph(len(masses),masses,r_all)
g_all.SetMarkerStyle(20)
g_all.SetMarkerColor(kBlack)
g_all.SetLineWidth(2)
g_all.SetLineStyle(1)
g_all.SetLineColor(kBlack)
g_all.GetXaxis().SetTitle("gg resonance mass [" + ("TeV" if useTeV else "GeV") + "]")
g_all.GetYaxis().SetTitle("Limit ratio")
g_all.GetYaxis().SetTitleOffset(1.1)
g_all.GetYaxis().SetRangeUser(0.5,4.5)
#g_all.GetXaxis().SetNdivisions(1005)

g_lumi = TGraph(len(masses),masses,r_lumi)
g_lumi.SetMarkerStyle(27)
g_lumi.SetMarkerColor(6)
g_lumi.SetLineWidth(2)
g_lumi.SetLineStyle(6)
g_lumi.SetLineColor(6)

g_jes = TGraph(len(masses),masses,r_jes)
g_jes.SetMarkerStyle(25)
g_jes.SetMarkerColor(kRed)
g_jes.SetLineWidth(2)
g_jes.SetLineStyle(5)
g_jes.SetLineColor(kRed)

g_jer = TGraph(len(masses),masses,r_jer)
g_jer.SetMarkerStyle(26)
g_jer.SetMarkerColor(7)
g_jer.SetLineWidth(2)
g_jer.SetLineStyle(6)
g_jer.SetLineColor(7)

g_trig = TGraph(len(masses),masses,r_trig)
g_trig.SetMarkerStyle(24)
g_trig.SetMarkerColor(kGreen+2)
g_trig.SetLineWidth(2)
g_trig.SetLineStyle(2)
g_trig.SetLineColor(kGreen+2)

g_allexbkg = TGraph(len(masses),masses,r_allexbkg)
g_allexbkg.SetMarkerStyle(23)
g_allexbkg.SetMarkerColor(42)
g_allexbkg.SetLineWidth(2)
g_allexbkg.SetLineStyle(4)
g_allexbkg.SetLineColor(42)

g_bkg = TGraph(len(masses),masses,r_bkg)
g_bkg.SetMarkerStyle(22)
g_bkg.SetMarkerColor(kBlue)
g_bkg.SetLineWidth(2)
g_bkg.SetLineStyle(3)
g_bkg.SetLineColor(kBlue)

c = TCanvas("c", "",800,800)
c.cd()

g_all.Draw("ALP")
g_lumi.Draw("LP")
g_jes.Draw("LP")
g_jer.Draw("LP")
g_trig.Draw("LP")
g_allexbkg.Draw("LP")
g_bkg.Draw("LP")

legend = TLegend(.40,.60,.60,.90)
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetFillStyle(0)
legend.SetTextFont(42)
legend.SetTextSize(0.035)
legend.SetMargin(0.20)
legend.SetHeader("Observed limit ratio")
legend.AddEntry(g_all, "All syst / Stat-only","lp")
legend.AddEntry(g_bkg, "Bkg syst / Stat-only","lp")
legend.AddEntry(g_allexbkg, "All except bkg syst / Stat-only","lp")
legend.AddEntry(g_trig, "Trigger syst / Stat-only","lp")
legend.AddEntry(g_jes, "JES syst / Stat-only","lp")
legend.AddEntry(g_jer, "JER syst / Stat-only","lp")
legend.AddEntry(g_lumi, "Lumi syst / Stat-only","lp")

legend.Draw()

#l1 = TLatex()
#l1.SetTextAlign(12)
#l1.SetTextFont(42)
#l1.SetNDC()
#l1.SetTextSize(0.04)
#l1.DrawLatex(0.18,0.43, "CMS Preliminary")
#l1.DrawLatex(0.18,0.35, "#intLdt = 5 fb^{-1}")
#l1.DrawLatex(0.19,0.30, "#sqrt{s} = 7 TeV")
#l1.DrawLatex(0.18,0.25, "|#eta| < 2.5, |#Delta#eta| < 1.3")
#l1.DrawLatex(0.18,0.20, "Wide Jets")

#draw the lumi text on the canvas
CMS_lumi.extraText = "Preliminary"
CMS_lumi.lumi_sqrtS = "1.9 fb^{-1} (13 TeV)" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)
iPos = 11
iPeriod = 0

CMS_lumi.CMS_lumi(c, iPeriod, iPos)

c.SetGridx()
c.SetGridy()

c.SaveAs('xs_limit_ratio_breakdown_DijetLimitCode_gg_Run2_Scouting_13TeV_DATA_1914_invpb.eps')
